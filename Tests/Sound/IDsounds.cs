﻿/*
 * Created by SharpDevelop.
 * User: hhazan01
 * Date: 08/06/2010
 * Time: 20:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using SoundCatcher;
using Utils_Functions;
using Liquid;

namespace IDsounds
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class IDsounds
	{
		
		public static void MainProgram(string[] args)
		{
			globalParam Param = new globalParam();
			
			//cheking howmeny files need to be leadnd
			string path=@Param.soundFiles_toLearn[0];
			DirectoryInfo di = new DirectoryInfo(path);
			DirectoryInfo[] ListDirec = di.GetDirectories();
			int Num_Files_Learn=0;
			foreach(DirectoryInfo Dir in ListDirec)
			{
				FileInfo[] rgFiles = Dir.GetFiles("*.wav");
				Num_Files_Learn+= rgFiles.Length;
			}
			//...
			
			// Init the LSM Network
			Console.WriteLine("Creating Liquid Unit for Right Channle");
			Liquid.LSM RNet = new Liquid.LSM(ref Param);
			Console.WriteLine("Creating Liquid Unit for Left Channle");
			Liquid.LSM LNet = new Liquid.LSM(ref Param);
			// finish Init and creating Network

			// init the Detector
			Console.WriteLine("Creating a Readout Unit for Right Channle");
			ReadOut_Detector[] RightReadOutUnit = new ReadOut_Detector[ListDirec.Length];
			Console.WriteLine("Creating a Readout Unit for Right Channle");
			ReadOut_Detector[] LeftReadOutUnit = new ReadOut_Detector[ListDirec.Length];
			for (int i = 0; i < ListDirec.Length; i++) {
				RightReadOutUnit[i]= new ReadOut_Detector(RNet.NetOutputSize,((int) Math.Round(RNet.NetOutputSize*0.03)),1,ref Param);
				LeftReadOutUnit[i] = new ReadOut_Detector(RNet.NetOutputSize,((int) Math.Round(RNet.NetOutputSize*0.03)),1,ref Param);
			}
			// finish the Detector
			
			Utils_Functions.Manipulation_On_Inputs soundToSpikes = new Manipulation_On_Inputs();
			
			globalParam.Data[] RData = new globalParam.Data[Num_Files_Learn];
			globalParam.Data[] LData = new globalParam.Data[Num_Files_Learn];
			int fileCounter=0;
			for(int counter = 0 ; counter < ListDirec.Length ; counter++)
			{
				FileInfo[] rgFiles = ListDirec[counter].GetFiles("*.wav");
				for (int i = 0; i < rgFiles.Length ; i++) {
					
					int[][] RightChannle,LeftChannle;
					Console.WriteLine("Loading:"+rgFiles[i].FullName);
					loadWaveFile(rgFiles[i].FullName,out RightChannle,out LeftChannle,RNet.InputSize,Param.MaxLevel);
					
					soundToSpikes.FrequencyFFTtoOnary(ref Param,ref RData[fileCounter], ref RightChannle);
					soundToSpikes.FrequencyFFTtoOnary(ref Param,ref LData[fileCounter], ref LeftChannle);
//					soundToSpikes.FrequencyFFTtoDirectInput(ref Param,ref RData[fileCounter], ref RightChannle);
//					soundToSpikes.FrequencyFFTtoDirectInput(ref Param,ref LData[fileCounter], ref LeftChannle);
					RData[fileCounter].Target[0] = counter;
					LData[fileCounter].Target[0] = counter;
					fileCounter++;
				}
			}
			
			Console.WriteLine("Running Liquid... ");
			Console.WriteLine("Running LSM on Learining Vector");
			double[][,] Right_LiquidOutput_InputUnits_Learn;
			double[][,] Right_LiquidOutput_OutputUnits_Learn;
			RNet.run( ref RData , out Right_LiquidOutput_OutputUnits_Learn, out Right_LiquidOutput_InputUnits_Learn, 1, 0.0 , ref Param);
			double[][,] Left_LiquidOutput_InputUnits_Learn;
			double[][,] Left_LiquidOutput_OutputUnits_Learn;
			LNet.run( ref LData , out Left_LiquidOutput_OutputUnits_Learn, out Left_LiquidOutput_InputUnits_Learn, 1, 0.0 , ref Param);
			
//			// ploting the activity of the liquid
//			for (int i = 0 ; i <Right_LiquidOutput_OutputUnits_Learn.Length ; i++){
//				plotxy p = new plotxy();
//				p.loadData(ref Right_LiquidOutput_OutputUnits_Learn[i],ref Right_LiquidOutput_InputUnits_Learn[i],ref Param );
//				Application.Run( p );
//				Application.Exit();
//			}
//
//			// ploting the activity of the liquid
//			for (int i = 0 ; i <Left_LiquidOutput_OutputUnits_Learn.Length ; i++){
//				plotxy p = new plotxy();
//				p.loadData(ref Left_LiquidOutput_OutputUnits_Learn[i],ref Left_LiquidOutput_InputUnits_Learn[i],ref Param );
//				Application.Run( p );
//				Application.Exit();
//			}
//
			
			
			// Training //
			double LastReturnError=0;
			for (int i = 0; i < RightReadOutUnit.Length ; i++) {
				Console.WriteLine("---------------------------"+i.ToString()+"-----------------------------------");
				double[] targetLearnData;
				return_Targets(RData,out targetLearnData,i,Param.Readout_Negative);
				Console.Write("Trainning Readout Unit on Right Liquid ");
				RightReadOutUnit[i].StratTrain(ref Right_LiquidOutput_OutputUnits_Learn,ref targetLearnData,ref LastReturnError ,ref Param);
				Console.WriteLine(" Average Training Error:"+LastReturnError.ToString());
				
				
				return_Targets(LData,out targetLearnData,i,Param.Readout_Negative);
				Console.Write("Trainning Readout Unit on Left Liquid");
				LeftReadOutUnit[i].StratTrain(ref Left_LiquidOutput_OutputUnits_Learn,ref targetLearnData,ref LastReturnError ,ref Param);
				Console.WriteLine(" Average Training Error:"+LastReturnError.ToString());
			}
			
			
			
			// Testing //
			
			//cheking howmeny files need to be Test
			path=@Param.soundFiles_toTest[0];
			di = new DirectoryInfo(path);
			ListDirec = di.GetDirectories();
			int Num_Files_Test=0;
			foreach(DirectoryInfo Dir in ListDirec)
			{
				FileInfo[] rgFiles = Dir.GetFiles("*.wav");
				Num_Files_Test+= rgFiles.Length;
			}
			//...
			
			RData = new globalParam.Data[Num_Files_Test];
			LData = new globalParam.Data[Num_Files_Test];
			fileCounter=0;
			for(int counter = 0 ; counter < ListDirec.Length ; counter++)
			{
				FileInfo[] rgFiles = ListDirec[counter].GetFiles("*.wav");
				for (int i = 0; i < rgFiles.Length ; i++) {
					
					int[][] RightChannle,LeftChannle;
					Console.WriteLine("Loading:"+rgFiles[i].FullName);
					loadWaveFile(rgFiles[i].FullName,out RightChannle,out LeftChannle,RNet.InputSize,Param.MaxLevel);
					
					soundToSpikes.FrequencyFFTtoOnary(ref Param,ref RData[fileCounter], ref RightChannle);
					soundToSpikes.FrequencyFFTtoOnary(ref Param,ref LData[fileCounter], ref LeftChannle);
//					soundToSpikes.FrequencyFFTtoDirectInput(ref Param,ref RData[fileCounter], ref RightChannle);
//					soundToSpikes.FrequencyFFTtoDirectInput(ref Param,ref LData[fileCounter], ref LeftChannle);
					RData[fileCounter].Target[0] = counter;
					LData[fileCounter].Target[0] = counter;
					fileCounter++;
				}
			}
			
			Console.WriteLine("Running Liquid... ");
			Console.WriteLine("Running LSM on Learining Vector");
			RNet.run( ref RData , out Right_LiquidOutput_OutputUnits_Learn, out Right_LiquidOutput_InputUnits_Learn, 1, 0.0 , ref Param);
			LNet.run( ref LData , out Left_LiquidOutput_OutputUnits_Learn, out Left_LiquidOutput_InputUnits_Learn, 1, 0.0 , ref Param);

			
			for (int i = 0; i < RightReadOutUnit.Length ; i++) {
				Console.WriteLine("---------------------------"+i.ToString()+"-----------------------------------");
				double[] targetLearnData;
				double[][] DetectorOutput;
				return_Targets(RData,out targetLearnData,i,Param.Readout_Negative);
				RightReadOutUnit[i].StratTesting(ref Right_LiquidOutput_OutputUnits_Learn, out DetectorOutput,ref Param);

				double[] average = new double[DetectorOutput.Length];
				for (int t = 0; t < DetectorOutput.Length; t++) {
					double sum = 0; int counter=0;
					for (int x = 0; x < DetectorOutput[t].Length; x++) {
						if ((RightReadOutUnit[i].Window_Accuracy.Length>1)&&(RightReadOutUnit[i].Window_Accuracy[x]<0.55)) continue;
						sum+= DetectorOutput[t][x];
						counter++;
					}
					average[t] = sum/counter;
				}
				for(int t=0 ; t<average.Length ; t++){
					Console.WriteLine("Right Target\t= {0}, Detector degree of correction\t = {1}",targetLearnData[t],average[t]);
				}
				
				return_Targets(LData,out targetLearnData,i,Param.Readout_Negative);
				LeftReadOutUnit[i].StratTesting(ref Left_LiquidOutput_OutputUnits_Learn, out DetectorOutput,ref Param);

				average = new double[DetectorOutput.Length];
				for (int t = 0; t < DetectorOutput.Length; t++) {
					double sum = 0; int counter=0;
					for (int x = 0; x < DetectorOutput[t].Length; x++) {
						if ((RightReadOutUnit[i].Window_Accuracy.Length>1)&&(RightReadOutUnit[i].Window_Accuracy[x]<0.55)) continue;
						sum+= DetectorOutput[t][x];
						counter++;
					}
					average[t] = sum/counter;
				}
				for(int t=0 ; t<average.Length ; t++){
					Console.WriteLine("Left Target\t= {0}, Detector degree of correction\t = {1}",targetLearnData[t],average[t]);
				}
			}
			
			Console.WriteLine("Finish");
			Console.ReadKey();

		}//-----------------------------------------------------------------------------------------------
		
		
		public static void return_Targets(globalParam.Data[] input,out double[] TargetVoxels,int task,int Negative){
			int size = input.Length;
			TargetVoxels = new double[size];
			for( int i = 0 ; i < size ; i++){
				TargetVoxels[i] = (input[i].Target[0]==task)? 1 : Negative ;
			}
			
		}
		
		public static void loadWaveFile(string file,out int[][]Left,out int[][] Right,int numOfInputUnits,int inputRange)
		{
			WaveStream wavStream = new WaveStream(new FileStream(file,FileMode.Open));
			byte[] wave = new byte[wavStream.Length];
			int countRead = wavStream.Read(wave,0,wave.Length);
			wavStream.Close();
			
			AudioFrame AudioProcess = new AudioFrame(false);
			
			int count=0,size = 16384; //16bit
			int temp = (wave.Length/size) +1 ;
			Left = new int[temp][];
			Right = new int[temp][];
			
			for(int i = 0 ; i < Left.Length ; i++)
			{
				byte[] buf = new byte[size];
				int intCount=0;
				for(int t = i ; t < (i+size) ; t++){
					if (t<wave.Length) buf[intCount]= wave[t];
					else buf[intCount]=0;
					intCount++;
				}
				
				AudioProcess.Process(ref buf);
				
				AudioProcess.RenderFrequencyDomainLeft(wavStream.Format.nSamplesPerSec,out Left[count],numOfInputUnits,inputRange);
				AudioProcess.RenderFrequencyDomainRight(wavStream.Format.nSamplesPerSec,out Right[count],numOfInputUnits,inputRange);
				
				count++;
			}
			
		}//-----------------------------------------------------------------------------------------------
		
	}
}
