using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable()]
public class globalParam
{
	//Misc
	public int iteration = 0; // mean the Voxel Number that the thread is learinig\testing
	public int Linux_OR_Windows; // 1= Windows(Debug) , 2 =Linux(Full Run)
	public int ThreadNum;
	
	// MRI Input files
	public string InputDir;
	public string OutputDir = @Directory.GetCurrentDirectory()+"//Results_Combination_";
	public string inputFiles = "block-detrCW2-zscored-5chunks-data.txt";
	public string SessionFile = "block-detrCW2-zscored-5chunks-chunks.txt";
	public string StimulyFile = "block-detrCW2-zscored-5chunks-labels.txt";
	public int IgnoreUntilLine; // How many lines to ignore in the labels files (the first fixetion..)
	public int NumOfVoxels;
	public double StreamFactor = 50;
	public int howMenyMoreVoxles = 20;
	
	// Sound Files
	public string[] soundFiles_toLearn = new System.String[]{
		@"D:\Rimon.Docs\2010\C#\Files to Process\Sound\Learn"};
	public string[] soundFiles_toTest = new System.String[]{
		@"D:\Rimon.Docs\2010\C#\Files to Process\Sound\Test"};
	public int MaxLevel = 50;
		
	public int TestInEachSession;
	//General Input Tranformation
	public int numbersBeforeDot = 4;
	public int numbersAfterDot = 10;
	
	
	//Damage in the LSM
	public int Repetition = 100;
	public int Numbers_of_Inputs = 20;
	public int Numbers_of_Inputs_For_Activity_Check=3;
	public string DirName = @Directory.GetCurrentDirectory()+@"//Noise=";
	public int NoisePercent = 11;
	public double LSM_Damage;
	
	
	[Serializable()]
	public struct DataStruct{
		public double[,] Input;
		public int Tag;
		public double Target;
	}
	[Serializable()]
	public struct Data{
		public double[][] Input;
		public int Tag;
		public double[] Target;
	}
	public Data[] LearnData,TestData;
	
	public int HowMenySessionsToTest=1;
	public int[] NumberOfSession;
	public int NumberOfTestIneachSesstion;
	public int NumberOfTimeInEachTest;
	public int[][] LearnGroup;
	public int[][] TestGrpup;
	public int CurrentGroup;
	public double[][] Groups;
	

	// parameters of Neurons in the Liquid and Liquid structure
	public int IncreaseSelectedChanceBy = 2 ;
	public int Number_Of_Neurons = 243 ;
	public double GeneralConnectivity = 0.05; // Maass configuration is 15%(0.15) ref 'make_liquid.m' and 'http://www.lsm.tugraz.at/circuits/usermanual/node10.html'
	public int Connections;
	
	// 0 = random , 1 = Old Mathod , 2 = FeedForward with Hubs, 3 = Maass et al. , 4 = Power Law on groups
	// 5 = Uncorrelated Scale Free , 6 = Uncorrelated Scale Free Powerlaw , 7 = Power Law Selections
	// 8 = Two Ways Pawer Law , 9 = Two Wayes Linear Descent
	public int Liquid_Architecture = 0 ;
	public int Liquid_Readout_Units = 1 ;  // 0 = random 1 = minimum input/output but bigger then zero
	public int Liquid_Update_Sync_Or_ASync = 0 ; // 0 = Synchrony update , 1 = Asynchrony update (random order)
	
	// Mathod 1 - Old Mathod
	public int Min_Neuron_Connection = 2 ; // random in or out
	public int Min_Group_Connections = 1 ; // in and out
	public int Group_size = 3;  // number of nurons need to be divided by Group Size
	//Mathod 2, 4, 8
	public int Group_size_Min = 3;
	public int Group_size_Max = 6;
	public int[] Group_interconnections = new int[2]{2,3}; //To how many one neuron that in the group will be connected other neurons in the group? min,max = Group Size - 1
	//Mathod 3 Maass et al.
	public int[] Maass_column = new int[3]{3,3,27};
	public int Maass_Lambda = 2; // Maass parameters are 0,2,4,8
	
	public double LSM_Percent_Of_Negative_Weights = 0.2; // Mass Parameter is 20%
	public double LSM_Input_Percent_Connectivity = 0.3; // Maass Parameter is 30%
	public double LSM_Max_Init_Weight_NegN;
	public double LSM_Min_Init_Weight_NegN;
	public double LSM_Max_Init_Weight_PosN;
	public double LSM_Min_Init_Weight_PosN;
	
	
	// Singal Neuron parameters
	public int Neuron_Model = 3 ; // 1= LIF 2=Izhikevich 3=LIF+Hananel add-ons 4=McCulloch�Pitts
	//0 = no for all,  1 = yes the Threshold will be 1/2 of the input, 2 = init like 1 after it will be Average of x cycles
	public int Proportional_Threshold = 0 ;  // proportional? 0 = no , 1 = yes
	public double Neuron_Threshold_Proportion; // this is relevent if proportional==yes
	public double Neuron_Spike;
	public double Neuron_Threshold;
	public double initV;
	public int Neuron_Spike_or_Volt = 1;  // 1 = spike, 0 = Volt
	public double decayFactor;
	public int Neuron_Min_Delay =1;
	public int Neuron_Max_Delay =1;
	public int Neuron_LTP_window = 17 ;
	public int Neuron_LTD_window = 34 ;
	public double Neuron_LTP = 0.0312 ;
	public double Neuron_LTD = 0.85 * 0.0312 ;
	
	// MODEL
	// 1 = Perceptron
	// 2 = ADELINE
	// 3 = Regilar Back-Propagation
	// 4 = Manhattan Back-Propagation
	// 5 = ResilientPropagation Back-Propagation
	// 6 = Scaled Conjugate Gradient
	public int ReadOut_Unit =5;
	// Mode
	// the Liquid output divide to windows with distance between windows
	// 1 size of input = size of the vertical length of the window  (thet equall to the read out unit)
	// 2 size of input = size of the horizontal length of the window
	public int Readout_Unit_Mode=2;
	
	// Readout Unit parapeters
	public int LSM_Runing_Interval = 500;
	public int[] LSM_Ratio_Of_Input_Interval = new int[]{1,1} ;  // {1,10} = 1 input in every 10 cycle of the liquid
	public int silence_or_repeted_Input_between_ratio = 1; // 0 = silent 1 = repetition of the previous input
	public int LSM_1sec_interval = 1000;
	public int LSM_AdjustmentTime= 50 ;
	public int numOfReadOutUnits = 2 ; // 1 = only one readout to all window, 2 = every window goes to defrent readout
	public int How_Many_Windows;
	public int ReadoutU_Window_Size = 1 ;
	public int ReadoutU_Disctance_Between_Windows = 50;
	public int ReadoutU_epoch = 50;
	public double Readout_Max_Error = 0.1;
	public int Readout_Negative = -1;
	public double ReadOut_Unit_HiddenLayerSize = 0.3 ; // 0.3 = 30% from Liquid output size
	public int ReadOut_Unit_outputSize = 1;
	
	// Activity Test in Liquid
	public int Activity_Test_Running_Time = 250;
	public int Activity_Test_Time_Between_Inputs = 100;
	public int Activity_Test_Greace_Time = 200;

	
	public globalParam(){
		initialization();
	}
	//----------------------------------------------------------------------------------
	
	public void initialization(){
		//--------------MRI Param------------------------
		/// Inputs:
		/// 1000000 =>  Fixation
		/// 100000  =>  F100 - Full face Normal contrast
		/// 10000   =>  F008 - Full face Low contrast
		/// 1000	=>	S100 - fuzzy face Normal contrast
		/// 100		=>	S008 - fuzzy face Low contrast
		/// 10		=>	H100 - Half face Normal contrast
		/// 1		=>	H008 - Half face Low contrast
		this.Groups = new double[2][];
		this.Groups[0] = new double[]{  1 ,100000};
		this.Groups[1] = new double[]{  -1 ,100};
		
		this.IgnoreUntilLine=3; // How many lines to ignore in the labels files (the first fixetion..)
		//--------------------------------------
		
		
		//---------------------Operating System patams--------------------------------
		System.OperatingSystem osInfo = System.Environment.OSVersion;
		if ( osInfo.Platform== PlatformID.Win32NT)
		{
			// Windows (debug)
			Linux_OR_Windows=2;
			this.InputDir = @"D:\Rimon.Docs\2010\C#\Files to Process\fmri\";
			ThreadNum = 1;
		}else{
			// Linux
			Linux_OR_Windows=1;
			this.InputDir = @"//home//hhazan01//2009//Italy//fmri//";
			//Cannot creat more then 255 zombies on Mono JIT compiler version 2.4.2.3
			ThreadNum = 12;
			if (ThreadNum<=1) ThreadNum=1;
		}
		//--------------------------------------------
		
		
		//------------------Read Out Params-------------------
		this.How_Many_Windows = (this.LSM_Runing_Interval-this.LSM_AdjustmentTime)/(this.ReadoutU_Window_Size+this.ReadoutU_Disctance_Between_Windows);
		//--------------------------------------------
		
		//----------------Neuron Modle-------------------------------------
		if (this.Neuron_Model==4){ // McCulloch�Pitts
			decayFactor = 0.5;
			this.Neuron_Spike = 1 ;
			this.Neuron_Threshold = 1 ;
			this.Neuron_Threshold_Proportion = 0.5; // 0.5 meen 1/2 of the inputs have spike then fire
			this.initV = 0 ;
			this.LSM_Max_Init_Weight_NegN = 0.55; //1.01;
			this.LSM_Min_Init_Weight_NegN = 0.5; //1;
			this.LSM_Max_Init_Weight_PosN = 0.55; //1.01;
			this.LSM_Min_Init_Weight_PosN = 0.5; //1;
		}else{
			decayFactor = 0.5; // LIF_HH(3) 0.01 = 99% decay 1 = 0% decay
			this.Neuron_Spike = 80 ;
			this.Neuron_Threshold = 30 ;
			this.Neuron_Threshold_Proportion = 0.5 ;// 0.5 meen 1/2 of the inputs have spike then fire
			this.initV = -65.0 ;
			this.LSM_Max_Init_Weight_NegN = 0.55; //1.01;
			this.LSM_Min_Init_Weight_NegN = 0.5; //1;
			this.LSM_Max_Init_Weight_PosN = 0.55; //1.01;
			this.LSM_Min_Init_Weight_PosN = 0.5; //1;
		}
		//-----------------------------------------------------
		
		//----------------Architecture-------------------------------------
		this.Connections = (int) Math.Floor(Number_Of_Neurons*(Number_Of_Neurons-1)*this.GeneralConnectivity);

		//-----------------------------------------------------
	}//----------------------------------------------------------------------------------
	
	
	public globalParam copy(){
		globalParam source = (globalParam) this.MemberwiseClone();
		return(source);
	}//----------------------------------------------------------------------------------
	
	public void save(){
		Stream fileStream = new FileStream("obj"+this.iteration.ToString()+".dat", FileMode.Create,FileAccess.ReadWrite, FileShare.None);
		BinaryFormatter binaryFormater = new BinaryFormatter();
		binaryFormater.Serialize(fileStream,this.copy());
		fileStream.Flush();
		fileStream.Close();
	}//----------------------------------------------------------------------------------
	
	public void save(string filename){
		Stream fileStream = new FileStream("obj"+filename+".dat", FileMode.Create,FileAccess.ReadWrite, FileShare.None);
		BinaryFormatter binaryFormater = new BinaryFormatter();
		binaryFormater.Serialize(fileStream,this.copy());
		fileStream.Flush();
		fileStream.Close();
	}//----------------------------------------------------------------------------------
	
	public int[] ConvertStringToInt(string[] input){
		int size = input.Length;
		int[] output = new int[size];
		for(int i = 0 ; i < size ; i++) int.TryParse(input[i],out output[i]);
		return output;
	}//----------------------------------------------------------------------------------
	
	public globalParam load(string args){
		Stream fileStream = new FileStream(args, FileMode.Open,FileAccess.Read, FileShare.None);
		BinaryFormatter binaryFormater = new BinaryFormatter();
		globalParam Param = (globalParam) binaryFormater.Deserialize(fileStream);
		fileStream.Close();
		return Param;
	}//----------------------------------------------------------------------------------
	
}


//----------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

[Serializable()]
public class cumulateor{
	double[] members;
	
	public cumulateor(){
		this.members = new double[0];
	}
	
	public double Return_Sum(){
		double sum=0;
		for ( long i = 0 ; i < this.members.LongLength ; i++ ) { sum+=this.members[i];}
		return sum;
	}
	public long ReturnMembers(){ return this.members.LongLength;}
	
	public void Add(double number){
		int length = this.members.Length;
		Array.Resize(ref this.members,length+1);
		this.members[length]=number;
	}
	
	public bool dec(double number){
		long counter=0;
		double[] temp = new double[this.members.LongLength-1];
		int flag=0;
		for ( long i = 0 ; i < this.members.LongLength ; i++ ) {
			if (number!= this.members[i]) { temp[counter]=this.members[i]; counter++;}
			else if ((number == this.members[i]) && (flag==0)){flag=1; continue;}
			else if ((number == this.members[i]) && (flag==1)){temp[counter]=this.members[i]; counter++;}
		}
		this.members = temp;
		// return 1 = seccsess , 0 = failed
		return(counter==this.members.LongLength);
	}
	
	public double Return_Average(){
		if (this.members.LongLength==0) return 0;
		else return (this.Return_Sum()/this.members.LongLength);
	}
	
	public double Return_Standard_Deviation(){
		double sum = 0;
		double avg = this.Return_Average();
		
		for ( long i = 0 ; i < this.members.LongLength ; i++ ) {
			sum+=Math.Pow((this.members[i]-avg),2);
		}
		
		if ( this.members.LongLength==0) sum*=0;
		else sum*=1/(double)this.members.LongLength;
		
		return (Math.Sqrt(sum));
	}
	
	public double Return_min(){
		if ( this.members.Length==0) {return 0;}
		double min = this.members[0];
		for ( long i = 0 ; i < this.members.LongLength ; i++ ) {
			if (this.members[i]<min) min = this.members[i];
		}
		
		return min;
	}
	
	public double Return_max(){
		if ( this.members.Length==0) {return 0;}
		double max = this.members[0];
		for ( long i = 0 ; i < this.members.LongLength ; i++ ) {
			if (this.members[i]>max) max = this.members[i];
		}
		
		return max;
	}
	
}