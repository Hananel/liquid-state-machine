﻿/*
 * Created by SharpDevelop.
 * User: hhazan01
 * Date: 27/04/2009
 * Time: 16:10
 *
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Inputs;
using Liquid;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;



namespace DamageLSM
{
	class Program
	{
		public static void MainProcess(string args)
		{
			globalParam Param = new globalParam();
			Param = Param.load(args);
			
			Directory.CreateDirectory(Param.DirName+Param.LSM_Damage.ToString());
			
			int iterationNumber=0;
			for (int repitaition=0 ; repitaition<Param.Repetition ; repitaition ++ ){
				Param.iteration =iterationNumber;

				Console.WriteLine("!Start the program! {0}",iterationNumber);

				// Init the LSM Network
				Liquid.LSM Net = new LSM(ref Param);
				// finish Init and creating Network

				//-----------------------------------------------------
				// Create Input
				DamageLSM.Input LearnVec = new Input(Param.Numbers_of_Inputs);
				LearnVec.SetInputToRandom(Param.LSM_Runing_Interval,Param.Neuron_Spike,Param.Readout_Negative);
				// Finish Create Input

				//open output files..
				string tempFileName = iterationNumber.ToString();
				string tempFileName2 = Param.LSM_Damage.ToString();

				if (Directory.Exists (@Param.DirName+tempFileName2)==false){
					Directory.CreateDirectory(Param.DirName+Param.LSM_Damage.ToString());
				}
				TextWriter tw = new StreamWriter(@Param.DirName+tempFileName2+@"//rsult-"+tempFileName+".txt");
				tw.WriteLine("Neurons in the network :"+Param.Number_Of_Neurons.ToString());

				//-----------------------------------------------------
				Console.WriteLine("Running LSM on Learning Vector");
				double[][,] LearnVecOutput;
				double[][,] LearnVecInputNeurons;
				Net.run( ref LearnVec.InputVec, out LearnVecOutput, out LearnVecInputNeurons, 1 , 0.0 , ref Param);
				
//				Net.PrintNetwork();
				
//				// ploting the firing activity of the liquid
//				plotxy plot= new plotxy();
//				plot.loadData(ref LearnVecOutput[1], ref LearnVecInputNeurons[1],ref Param);
//				Application.Run( plot );
//				Application.Exit();
//				plot = null;
				
				//--------------------------------------------------
				Console.WriteLine("Running LSM on Learning Vector with Noise Generator");
				double[][,] NoiseGeneratorLearnVecOutput;
				double[][,] NoiseGeneratorLearnVecInputNeurons;
				Net.run( ref LearnVec.InputVec, out NoiseGeneratorLearnVecOutput, out NoiseGeneratorLearnVecInputNeurons, 2, Param.LSM_Damage,ref Param);
				
//				// ploting the firing activity of the liquid
//				plot= new plotxy();
//				plot.loadData(ref NoiseGeneratorLearnVecOutput[1],ref NoiseGeneratorLearnVecInputNeurons[1],ref Param);
//				Application.Run( plot );
//				Application.Exit();
//				plot = null;

				//--------------------------------------------------
				Console.WriteLine("Running LSM on Learning Vector with Damage in the LSM");
				double[][,] DamageLearnVecOutput;
				double[][,] DamageLearnVecInputNeurons;
				Net.run( ref LearnVec.InputVec, out DamageLearnVecOutput, out DamageLearnVecInputNeurons,  3, Param.LSM_Damage,ref Param);
				
				//--------------------------------------------------
				Console.WriteLine("Running LSM on Combine Damage test with the Learning Vector");
				double[][,] CombineDamageOutput;
				double[][,] CombineDamageInputNeurons;
				Net.run( ref LearnVec.InputVec, out CombineDamageOutput,out CombineDamageInputNeurons, 4, Param.LSM_Damage,ref Param);
				// finish with the LSM on Vector
				
//			plot= new plotxy();
//			plot.loadData(ref DamageLearnVecOutput[1],ref DamageLearnVecInputNeurons[1],ref Param);
//			Application.Run( plot );
//			Application.Exit();
//			plot = null;

				//--------------------------------------------------
				Console.WriteLine("Running LSM on Test - Random Vectors");
				DamageLSM.Input TestVec = new Input(Param.Numbers_of_Inputs);
				TestVec.SetInputToRandom(Param.LSM_Runing_Interval,Param.Neuron_Spike,Param.Readout_Negative);
				double[][,] TestVecOutput;
				double[][,] TestInputNeurons;
				Net.run( ref TestVec.InputVec,  out TestVecOutput, out TestInputNeurons, 1, 0,ref Param);

				//--------------------------------------------------
				Console.WriteLine("Running LSM on Generalization with the Learning Vector");
				double[][,] GeneralizationOutput;
				double[][,] GeneralizationInputNeurons;
				Net.run( ref LearnVec.InputVec, out GeneralizationOutput,out GeneralizationInputNeurons, 0, Param.LSM_Damage,ref Param);
				// finish with the LSM on Vector
				
				//--------------------------------------------------
				Console.WriteLine("measure LSM For memoriy in Time....");
				Input TimeInput = new Input(Param.Numbers_of_Inputs_For_Activity_Check);
				
				TimeInput.initTimePatterns(Param.Neuron_Spike);
				double[][,] outputNuerons_T = new double[TimeInput.HowManyNumbers][,];
				double[][,] InputNeurons_T = new double[TimeInput.HowManyNumbers][,];
				double[][,] LiquidActivity = new double[TimeInput.HowManyNumbers][,];
				double[] Target = new double[TimeInput.HowManyNumbers];
				double[] Targets = new double[]{1, 0 };
				Net.run_TimeTest(ref TimeInput, ref Targets, ref outputNuerons_T,ref InputNeurons_T, ref Target,ref LiquidActivity,ref Param);

				//-----------------------------------------------------
				// init Back Propagation

				//BP bp2 = new BP(Net.NetOutputSize, Param.Numbers_of_Inputs*4, 1);
				ReadOut_Detector NNet = new ReadOut_Detector(Net.NetOutputSize, ((int) Math.Round(Net.NetOutputSize*0.03)), 1,ref Param);
				Net = null;
				
				Utils_Functions.Matrix_Arithmetic sum = new Utils_Functions.Matrix_Arithmetic();
				double[][] ReadOut;
				int Presult,Nresult,results;

				// Training Back Propagation BY Sliceing Time
				double[] targetLearnData,BPoutput;
				double LastReturnError =0;
				LearnVec.returnTargetData(out targetLearnData);
				NNet.StratTrain(ref LearnVecOutput, ref targetLearnData,ref LastReturnError,ref Param);

				// testing Back Propagation
				double middle = (1 + Param.Readout_Negative)/2;
				
				Console.WriteLine("Start Normal Testing Vectors");
				NNet.StratTesting(ref LearnVecOutput, out ReadOut,ref Param);
				BPoutput = sum.Vector_Average(ReadOut);
				Presult=0;Nresult=0;results=0;
				for(int i=0; i<BPoutput.GetLength(0);i++) {
					if ((BPoutput[i]>middle)&&(targetLearnData[i]>0)) { results++;}
					if ((BPoutput[i]<=middle)&&(targetLearnData[i]<=0)) { results++;}
					if (BPoutput[i]>middle) {Presult++;} else {Nresult++;}
				}
				tw.WriteLine("Results on learning Vectors : {0}/{1} ({2}+ , {3}-)",results,BPoutput.GetLength(0),Presult,Nresult);

				Console.WriteLine("Start Noise Generator Testing Vectors");
				NNet.StratTesting(ref NoiseGeneratorLearnVecOutput, out ReadOut,ref Param);
				BPoutput = sum.Vector_Average(ReadOut);
				Presult=0;Nresult=0;results=0;
				for(int i=0; i<BPoutput.GetLength(0);i++) {
					if ((BPoutput[i]>middle)&&(targetLearnData[i]>0)) { results++;}
					if ((BPoutput[i]<=middle)&&(targetLearnData[i]<=0)) { results++;}
					if (BPoutput[i]>middle) {Presult++;} else {Nresult++;}
				}
				tw.WriteLine("Results on Noise Generator : {0}/{1} ({2}+ , {3}-)",results,BPoutput.GetLength(0),Presult,Nresult);

				Console.WriteLine("Start Damage in LSM Testing Vectors");
				NNet.StratTesting(ref DamageLearnVecOutput, out ReadOut,ref Param);
				BPoutput = sum.Vector_Average(ReadOut);
				Presult=0;Nresult=0;results=0;
				for(int i=0; i<BPoutput.GetLength(0);i++) {
					if ((BPoutput[i]>middle)&&(targetLearnData[i]>0)) { results++;}
					if ((BPoutput[i]<=middle)&&(targetLearnData[i]<=0)) { results++;}
					if (BPoutput[i]>middle) {Presult++;} else {Nresult++;}
				}
				tw.WriteLine("Results on Damage in LSM : {0}/{1} ({2}+ , {3}-)",results,BPoutput.GetLength(0),Presult,Nresult);


				Console.WriteLine("Start Testing Random Vectors");
				NNet.StratTesting(ref TestVecOutput, out ReadOut,ref Param);
				BPoutput = sum.Vector_Average(ReadOut);
				Presult=0;Nresult=0;results=0;
				for(int i=0; i<BPoutput.GetLength(0);i++) {
					if ((BPoutput[i]>middle)&&(targetLearnData[i]>0)) { results++;}
					if ((BPoutput[i]<=middle)&&(targetLearnData[i]<=0)) { results++;}
					if (BPoutput[i]>middle) {Presult++;} else {Nresult++;}
				}
				tw.WriteLine("Results on Random Vectors : {0}/{1} ({2}+ , {3}-)",results,BPoutput.GetLength(0),Presult,Nresult);


				Console.WriteLine("Start Testing Generalization Vectors");
				NNet.StratTesting(ref GeneralizationOutput, out ReadOut,ref Param);
				BPoutput = sum.Vector_Average(ReadOut);
				Presult=0;Nresult=0;results=0;
				for(int i=0; i<BPoutput.GetLength(0);i++) {
					if ((BPoutput[i]>middle)&&(targetLearnData[i]>0)) { results++;}
					if ((BPoutput[i]<=middle)&&(targetLearnData[i]<=0)) { results++;}
					if (BPoutput[i]>middle) {Presult++;} else {Nresult++;}
				}
				tw.WriteLine("Results on Generalization Vectors : {0}/{1} ({2}+ , {3}-)",results,BPoutput.GetLength(0),Presult,Nresult);
				
				Console.WriteLine("Start Testing Combine Damage Test");
				NNet.StratTesting(ref CombineDamageOutput, out ReadOut,ref Param);
				BPoutput = sum.Vector_Average(ReadOut);
				Presult=0;Nresult=0;results=0;
				for(int i=0; i<BPoutput.GetLength(0);i++) {
					if ((BPoutput[i]>middle)&&(targetLearnData[i]>0)) { results++;}
					if ((BPoutput[i]<=middle)&&(targetLearnData[i]<=0)) { results++;}
					if (BPoutput[i]>middle) {Presult++;} else {Nresult++;}
				}
				tw.WriteLine("Results on Combine Damage Test : {0}/{1} ({2}+ , {3}-)",results,BPoutput.GetLength(0),Presult,Nresult);
				
				
				NNet =	null;
				
				tw.WriteLine(" Time Activity of the Liquid");
				for ( int i = 0 ; i < LiquidActivity.Length; i++ ) {
					tw.WriteLine("For Test Number " + i.ToString());
					for ( int t=0 ; t< LiquidActivity[i].GetLength(1) ; t++ ) {
						tw.Write(LiquidActivity[i][0,t].ToString()+"	");
					}
					tw.WriteLine("");
					for ( int t=0 ; t< LiquidActivity[i].GetLength(1) ; t++ ) {
						tw.Write(LiquidActivity[i][1,t].ToString()+"	");
					}
					tw.WriteLine("");
				}

//			// ploting the activity of the liquid
//			plotGraph p = new plotGraph();
//			p.loadData(ref LiquidActivity);
//			Application.Run( p );
//			Application.Exit();


				// close the stream
				tw.Flush();
				tw.Close();
				//-----------------------------------------------------
				iterationNumber++;
			}
			File.Delete(args);
		}


		
		[STAThread]
		public static void DmageLSM_Main(string[] args)
		{
			if (args.Length>0){
				if (args.Length==2){
					Reports.ConectivityTest(args[0]);
				}
				else
					MainProcess(args[0]);
			}
			else{
				globalParam Param = new globalParam();
				Process[] p;
				string[] pName;
				
				System.OperatingSystem osInfo = System.Environment.OSVersion;
				Console.WriteLine(osInfo.Platform);
				
				if ( osInfo.Platform== PlatformID.Win32NT) { Param.Linux_OR_Windows=2;}
				else {Param.Linux_OR_Windows=1;}
				
				if ( Param.Linux_OR_Windows==1) {
					p = new Process[Param.ThreadNum];
					pName = new string[Param.ThreadNum];
				}else{ // Debug
					p = new Process[1];
					pName = new string[1];
				}
				
				
				for (int NoisePercent = Param.NoisePercent ; NoisePercent>-1; NoisePercent--){
					Param.LSM_Damage = (double)NoisePercent/100;

					int flag =0,counter=0;
					while (flag==0) {
						if (!File.Exists(pName[counter])){
							Param.save(NoisePercent.ToString());
							pName[counter]="obj"+NoisePercent.ToString()+".dat";
							p[counter]= new Process();
							p[counter].StartInfo.FileName=@"mono Liquid\ Detector.exe";
							if (NoisePercent==Param.NoisePercent)
								p[counter].StartInfo.Arguments=pName[counter]+"  2";
							else
								p[counter].StartInfo.Arguments=pName[counter];
							System.Console.WriteLine("+++{0}+++",NoisePercent);

							if (Param.Linux_OR_Windows==1)
							{	p[counter].Start();
							}else{
								string arg = "obj"+NoisePercent.ToString()+".dat";
								if (NoisePercent==Param.NoisePercent){
									//Reports.ConectivityTest(arg); // for Dubuging
									File.Delete(arg);
									flag=1;
									continue;
								}
								MainProcess(arg); // for Dubuging
							}
							flag=1;
							p[counter].Dispose();
						}
						if ((counter+1)==Param.ThreadNum) {Thread.Sleep(5000); counter=0;}
						else counter++;
					}
					
				}
				
				DirectoryInfo di = new DirectoryInfo(Directory.GetCurrentDirectory());
				FileInfo[] rgFiles = di.GetFiles("*.dat");
				while (rgFiles.Length>0) {
					Console.WriteLine("Waiting....");
					Thread.Sleep(10000);
					rgFiles = di.GetFiles("*.dat");
				}
				Console.WriteLine("Finising....");
				Reports.Summing(ref Param);
			}
		}
	}
}