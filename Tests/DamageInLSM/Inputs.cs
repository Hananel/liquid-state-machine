/*
 * Created by SharpDevelop.
 * User: hhazan01
 * Date: 05/05/2009
 * Time: 10:50
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Utils_Functions;

namespace DamageLSM
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class Input
	{
		public globalParam.Data[] InputVec;
		double[][] InputVecTimeTest;
		long[] numbers,TimeInput;
		public int HowManyNumbers;
		
		public Input(int HowManyNumbers){
			this.HowManyNumbers=HowManyNumbers;
			this.InputVec = new globalParam.Data[HowManyNumbers];
			this.InputVecTimeTest = new double[HowManyNumbers][];
			this.numbers= new long[]{2154623452485,574328521581,2463518721842,9874863015411,123428625684,6874681313534,4681326,78768443543,3543534211,85432134,215452485,5742518,246358242,9605411,123425684,6874134,468741816,87634376,3532112158,8543499721};
			this.TimeInput = new long[]{5,11,22,25,44,64};
		}
		//--------------------------------------------------------
		
		public void SetInputToPattern(){
			for (int i=0 ; i<this.InputVec.Length ; i++ ) {
				this.InputVec[i].Input = new double[1][];
				string s = ToBinary(this.numbers[i]);
				this.InputVec[i].Input[0] = new double[s.Length];
				for (int t = 0 ; t < s.Length ; t++ ) {
					if (s[t].CompareTo('1')==1) this.InputVec[i].Input[0][t] = 1 ;
					else this.InputVec[i].Input[0][t] = 0 ;
				}
				this.InputVec[i].Tag = 0;
				if (i < (this.InputVec.Length/2)) this.InputVec[i].Target[0] = 0;
				else this.InputVec[i].Target[0] = 1;
			}
		}
		//--------------------------------------------------------
		
		public void returnTargetData(out double[] targetData){
			targetData = new double[this.InputVec.Length];
			for (int i=0 ; i<this.InputVec.Length ; i++ ) {
				targetData[i] = this.InputVec[i].Target[0];
			}
		}
		//--------------------------------------------------------
		
		public void initTimePatterns(double fire){
			for (int i=0 ; i<this.HowManyNumbers ; i++ ) {
				char[] bits = ToBinary(this.numbers[i]).ToCharArray();
				this.InputVecTimeTest[i] = new double[bits.Length];
				for (int t = 0 ; t < bits.Length ; t++ ) {
					if (bits[t].ToString()=="1") this.InputVecTimeTest[i][t] = fire ;
					else this.InputVecTimeTest[i][t] = 0 ;
//					this.InputVecTimeTest[i][t] = Convert.ToDouble(bits[t].ToString());
				}
			}
		}
		//--------------------------------------------------------
		
		public void SetInputToRandom(int size,double fire,int negativ){
			long tempSeed = (System.DateTime.Now.Ticks);
			Random rnd = new Random(Convert.ToInt32(tempSeed%int.MaxValue));
			size = size /1000+10;
			for (int i=0 ; i<this.InputVec.Length ; i++ ) {
				this.InputVec[i].Input = new double[1][];
				int bits = rnd.Next(size,size*10);
				this.InputVec[i].Input[0] = new double[bits];
				for (int t = 0 ; t < bits ; t++ ) {
					if (rnd.NextDouble()>0.5) this.InputVec[i].Input[0][t] = fire ;
					else this.InputVec[i].Input[0][t] = 0 ;
				}
				this.InputVec[i].Tag = 0;
				this.InputVec[i].Target = new double[1];
				if (i < (this.InputVec.Length/2)) this.InputVec[i].Target[0] = negativ;
				else this.InputVec[i].Target[0] = 1;
			}
		}
		//--------------------------------------------------------

		public int returnSizeOfVector(int number){
			return this.InputVec[number].Input.Length;
		}
		//--------------------------------------------------------
		
		public double[] returnInputAtPlace(int number,int place){
			
			return (this.InputVec[number].Input[place]);
		}
		//--------------------------------------------------------

		public double[] returnInput(int number){
			return this.InputVecTimeTest[number];
		}
		//--------------------------------------------------------
		
		static private int ToDecimal(string bin)
		{
			return Convert.ToInt32(bin,2);
		}
		//--------------------------------------------------------
		
		static private string ToBinary(Int64 Decimal)
		{
			// Declare a few variables we're going to need
			Int64 BinaryHolder;
			char[] BinaryArray;
			string BinaryResult = "";
			
			if ( Decimal==0) {
				BinaryResult="0";
			}

			while (Decimal > 0)
			{
				BinaryHolder = Decimal % 2;
				BinaryResult += BinaryHolder;
				Decimal = Decimal / 2;
			}
			// The algoritm gives us the binary number in reverse order (mirrored)
			// We store it in an array so that we can reverse it back to normal
			BinaryArray = BinaryResult.ToCharArray();
			Array.Reverse(BinaryArray);
			BinaryResult = new string(BinaryArray);

			return BinaryResult;
		}
		//--------------------------------------------------------
		
		
	}//---------------------
}
