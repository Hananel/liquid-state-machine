﻿/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 28/07/2010
 * Time: 10:20
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;

namespace LiquidHRF
{
	/// <summary>
	/// Description of MainLiquidHRF.
	/// </summary>
	public class LiquidHRF
	{
		double[][][] m_stimuliChunks;
		double[][][] m_voxelChunks;
		
		public static void MainLiquidHRF(string[] args)
		{				
			LiquidHRF slowTest = new LiquidHRF();
			slowTest.Run("C:\\LSM\\synthetic_data\\slowER_stimuli.txt", "C:\\LSM\\synthetic_data\\slowER_voxels.txt", 20);
			
//			Console.ReadKey();
			
		}
		
		public void Run(string stimuliPath, string voxelPath, int chunkSize)
		{
			// load simulation data
			if (!LoadData(stimuliPath, voxelPath, chunkSize))
				return;
			
			// divide chunks to train and test			
			int numOfTrainChunks = m_stimuliChunks.Length - m_stimuliChunks.Length/10;						
			// for each voxel:
				
			globalParam parameters = new globalParam();
			Liquid.LSM lsm = new Liquid.LSM(ref parameters);
			ReadOut_Detector readOutDetector = new ReadOut_Detector(lsm.NetOutputSize,((int) Math.Round(lsm.NetOutputSize*0.03)),1,ref parameters);
			
			for (int voxel = 0; voxel < m_voxelChunks[0].Length; voxel++ )
				Train(lsm, readOutDetector, voxel, numOfTrainChunks);
			
			// test LSM with test chunks/signals					
		}
		
		private bool LoadData(string stimuliPath, string voxelPath, int chunkSize) 
		{
			// load input sequences for 2 stimuli
			DataSet stimuli = new DataSet();
			if (!stimuli.Load(stimuliPath)) {
			    return false;		
			}
			// load signals for X voxels			
			DataSet voxels = new DataSet();
			if (!voxels.Load(voxelPath)) {
			    return false;		
			}			
			// divide stimuli input into chunks of 20 seconds
			m_stimuliChunks = stimuli.GetChunks(chunkSize);
			// divide voxels input into chunks of 20 seconds	
			m_voxelChunks = stimuli.GetChunks(chunkSize);
			
			return true;
		}
		
		private void Train(Liquid.LSM lsm, ReadOut_Detector readOutDetector, int voxelIdx, int chunkSize) 
		{						
			//lsm.run( ref RData , out Right_LiquidOutput_OutputUnits_Learn, out Right_LiquidOutput_InputUnits_Learn, 1, 0.0 , ref Param);
			
		}
	}
}
