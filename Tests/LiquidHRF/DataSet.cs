﻿/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 28/07/2010
 * Time: 10:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;

namespace LiquidHRF
{
	/// <summary>
	/// Description of DataSet.
	/// </summary>
	public class DataSet
	{
		double[][] m_dataValues;
		
		public DataSet() {}
		
		public bool Load(string dataPath)
		{
			if (!InitArraySize(dataPath))
				return false;
			
			int rowIdx = 0;
	        try {
	            using (StreamReader sr = new StreamReader(dataPath)) {
	                string line;
	                while ((line = sr.ReadLine()) != null) {
	                	if (line.StartsWith("#")) {
	                		continue;
	                	}
						
	                	string[] cols = line.Split('\t');
	                	int colIdx = 0;
						foreach (string col in cols) { 
							m_dataValues[colIdx++][rowIdx] = Convert.ToDouble(col);
						}	            						
						rowIdx++;
	                }
				}
	            return true;
	        }
	        catch (Exception e) 
	        {
	            // Let the user know what went wrong.
	            Console.WriteLine("Error: The data file could not be read.");
	            Console.WriteLine(e.Message);
	            return false;
	        }
				
		}		
		
		// number of chunks, number of cols in chunk, number of 
		public double[][][] GetChunks(int chunkSize)
		{
			int chunkCount = m_dataValues[0].Length / chunkSize;
			
			double[][][] chunks;
			int colCount = m_dataValues.Length;
			int rowCount = m_dataValues[0].Length;
			chunks = new double[chunkCount][][];
			for (int i=0; i<chunks.Length; i++) {
				chunks[i] = new double[colCount][];
				for (int j=0; j<colCount; j++) {
					chunks[i][j] = new double[rowCount];
				}
			}
			
			int chunkRowIdx = 0;
			for (int chunkIdx = 0; chunkIdx < chunkCount; chunkIdx++) {
				for (int i = 0; i< chunkCount; i++) {
					for (int j = 0; j < m_dataValues.Length; j++) {
						chunks[chunkIdx][j][i] = m_dataValues[j][chunkIdx*chunkSize+i];
					}
				}
			}
			
			return chunks;
		}
		
		private bool InitArraySize(string dataPath)
		{
            int rowCount = 0;
            int columnCount = 0;
	        try {
	            using (StreamReader sr = new StreamReader(dataPath)) {
	                string line;
	                while ((line = sr.ReadLine()) != null) {
	                	if (line.StartsWith("#")) {
	                		continue;
	                	}
	                	
	                	if (columnCount == 0) {
	                		columnCount = GetDataCount(line);
	                	}
	                	
	                	rowCount++;
	                }
	            }
	            
				if (columnCount == 0 || rowCount == 0) {
					Console.WriteLine("Error: The data file " + dataPath + " is empty.");
					return false;
				}
				
				m_dataValues = new double[columnCount][];
				for (int i=0; i<columnCount; i++)
					m_dataValues[i] = new double[rowCount];
	            return true;
	        }
	        catch (Exception e) 
	        {
	            // Let the user know what went wrong.
	            Console.WriteLine("Error: The data file could not be read.");
	            Console.WriteLine(e.Message);
	            return false;
	        }
		}
			
		private int GetDataCount(string line) 
		{
			string[] words = line.Split('\t');
			return words.Length;
		}
	}	
}
