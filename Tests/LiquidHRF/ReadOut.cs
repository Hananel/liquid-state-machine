﻿using System;
using System.Collections.Generic;

using NN_Pr;

using Encog.MathUtil.Error;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Activation;
using Encog.Neural.Data.Basic;
using Encog.Neural.NeuralData;
using Encog.Neural.NeuralData.Bipolar;
using Encog.Neural.Networks.Training;
using Encog.Neural.Data;
using Encog.Neural.Networks.Training.Simple;
using Encog.Neural.Networks.Training.Propagation.Gradient;
using Encog.Neural.Networks.Training.Propagation.Back;
using Encog.Neural.Networks.Training.Propagation.Manhattan;
using Encog.Neural.Networks.Training.Propagation.Resilient;
using Encog.Neural.Networks.Training.Propagation.SCG;
using Encog.Neural.Networks.Training.Strategy;
using Encog.Util.Banchmark;
using Encog.Util.Logging;

namespace LiquidHRF
{
    class ReadOut
    {

        private int m_netSize;
        private int m_inputSize;
        private int m_outputSize;

        BasicNetwork m_network;

        public ReadOut(int inputSize, int hiddenSize, int outputSize)
        {
            //Console.WriteLine("Initilaizing readout...");

            this.m_outputSize = outputSize;
            this.m_inputSize = inputSize;
            this.m_netSize = inputSize + hiddenSize + outputSize;
            this.m_network = new BasicNetwork();

            if (hiddenSize == 0) hiddenSize = 1;

            this.m_network.AddLayer(new BasicLayer(new ActivationTANH(), true, inputSize));
            this.m_network.AddLayer(new BasicLayer(new ActivationTANH(), true, hiddenSize));
            this.m_network.AddLayer(new BasicLayer(new ActivationLinear(), true, outputSize));
            this.m_network.Structure.FinalizeStructure();
            this.m_network.Reset();
        }

        public double Train(double[][,] inputRawData,
                            double[][] outputRawData,
                            double[][] outputIdealRawData,
                            globalParam parameters,
                            int numOfTrainChunks)
        {
            INeuralDataSet trainingSet = PrepareTrainingSet(inputRawData, outputRawData, numOfTrainChunks, parameters.Neuron_Threshold);
            INeuralDataSet idealSet = null;
            if (outputIdealRawData != null)
            {
                idealSet = PrepareTrainingSet(inputRawData, outputIdealRawData, numOfTrainChunks, parameters.Neuron_Threshold);
            }
            return StartTrainProcedure(trainingSet, idealSet, parameters, numOfTrainChunks);
        }

        public INeuralDataSet PrepareTrainingSet(double[][,] inputRawData,
                                                  double[][] outputRawData,
                                                  int numOfTrainChunks,  
                                                  double neuronThreshold)
        {
            if (inputRawData.Length == 0)
                return null;

            int numOfTimeSlots = inputRawData[0].GetLength(1);
            int numOfInputNeurons = inputRawData[0].GetLength(0);
            double[][] input = new double[numOfTrainChunks * numOfTimeSlots][];

            for (int i = 0; i < numOfTrainChunks; i++)
            {
                for (int j = 0; j < numOfTimeSlots; j++)
                {
                    int patternIdx = i * numOfTimeSlots + j;
                    input[patternIdx] = new double[numOfInputNeurons];
                    for (int k = 0; k < numOfInputNeurons; k++)
                    {
                        input[patternIdx][k] = inputRawData[i][k, j] > neuronThreshold ? 1 : 0;
                    }
                }
            }

            double[][] output = new double[numOfTrainChunks * outputRawData[0].Length][];
            for (int i = 0; i < numOfTrainChunks; i++)
            {
                for (int j = 0; j < outputRawData[0].Length; j++)
                {
                    output[outputRawData[0].Length * i + j] = new double[1];
                    output[outputRawData[0].Length * i + j][0] = outputRawData[i][j];
                }
            }

            //for (int i = 0; i < input.Length; i++)
            //{
           //     int idx = 0;
           //     for (int j = 0; j < input[0].Length; j++)
           //     {
           //         if (input[i][j] == 1) idx++;
           //         //Console.Write(input[i][j] == 1 ? "*" : "-");
           //     }
                //Console.WriteLine("");
                //Console.WriteLine("Vector " + i + ": " + output[i][0] + " " + idx);
          //  }
            
            return new BasicNeuralDataSet(input, output);
        }

        public double StartTrainProcedure(INeuralDataSet trainingSet,
                                          INeuralDataSet idealSet,  
                                          globalParam parameters,
                                          int numOfTrainChunks )
        {
            //Console.WriteLine("Start training " + numOfTrainChunks + "... ");
            ResilientPropagation train = new ResilientPropagation(m_network, trainingSet);
            int epoch = 1;
            double error = 0;
            do
            {
                train.Iteration();
                epoch++;
                error = GetError(train.Error, idealSet);
                //Console.WriteLine("Epoch #" + epoch.ToString() + " Error:" + error);
            } while ((epoch < parameters.ReadoutU_epoch) && error > parameters.Readout_Max_Error);
            double[][] results = StartTestProcedure(idealSet);
            double correlation = GetPearsonCorrelation(results[0], results[1]);
            //Console.WriteLine("Epoch #" + epoch.ToString() + " Error:" + error + " Correlation:" + correlation + " RMS: " + GetRMS(results[0], results[1]));        
            Console.WriteLine("TRAINED Pearson: " + correlation + "    Spearman: " + GetSpearmanCorrelation(results[0], results[1]) + "   RMS: " + GetRMS(results[0], results[1])); 
            return error;
        }

        public void Test(double[][,] inputRawData,
                         double[][] outputRawData,
                         globalParam parameters,
                         int numOfTrainChunks)
        {
            INeuralDataSet trainingSet = PrepareTrainingSet(inputRawData, outputRawData, numOfTrainChunks, parameters.Neuron_Threshold);
            if (trainingSet == null)
                return;

            double[][] results = StartTestProcedure(trainingSet);
            Console.WriteLine("TESTED Pearson: " + GetPearsonCorrelation(results[0], results[1]) + "    Spearman: " + GetSpearmanCorrelation(results[0], results[1]) + "   RMS: " + GetRMS(results[0], results[1]));
        }

        public double GetRMS(double[] x, double[] y)
        {
            double diff_xy_sqrt = 0;
            double diff_xy = 0;
            for (int i = 0; i < x.Length; i++)
            {
                diff_xy = Math.Abs(x[i] - y[i]);
                diff_xy_sqrt += diff_xy * diff_xy;
            }

            return Math.Sqrt(diff_xy_sqrt / x.Length);
        }

        public double GetPearsonCorrelation(double[] x, double[] y)
        {
            double mu_x = 0;
            double mu_y = 0;
            double mu_dev_x_y = 0;
            double mu_dev_x = 0;
            double mu_dev_y = 0;

            int startIdx = 0;
            int count = x.Length;

            for (int i = startIdx; i < startIdx + count; i++)
            {
                mu_x += x[i];
                mu_y += y[i];
                //Console.WriteLine(x[i] + "  " + y[i]);
            }

            mu_x = mu_x / count;
            mu_y = mu_y / count;

            for (int i = startIdx; i < startIdx + count; i++)
            {
                mu_dev_x += (x[i] - mu_x) * (x[i] - mu_x);
                mu_dev_y += (y[i] - mu_y) * (y[i] - mu_y);
                mu_dev_x_y += (x[i] - mu_x) * (y[i] - mu_y);
            }

            mu_dev_x = Math.Sqrt(mu_dev_x / (count-1));
            mu_dev_y = Math.Sqrt(mu_dev_y / (count - 1));
            mu_dev_x_y = mu_dev_x_y / (count - 1);

            return mu_dev_x_y / (mu_dev_x * mu_dev_y);
        }

        public double GetSpearmanCorrelation(double[] x, double[] y)
        {
            double[] x_sort = new double[x.Length];
            double[] y_sort = new double[y.Length];
            double[] x_rank = new double[x.Length];
            double[] y_rank = new double[y.Length];
            double dPowTotal = 0;

            Console.WriteLine("Results:");
            for (int i = 0; i < x.Length; i++)
            {
                x_sort[i] = x[i];
                y_sort[i] = y[i];
                Console.WriteLine(x[i] + " " + y[i]);
                x_rank[i] = -1;
                y_rank[i] = -1;
            }
            Console.WriteLine("Results - end");
            Array.Sort(x_sort);
            Array.Sort(y_sort);

            for (int i = 0; i < x_sort.Length; i++)
            {
                for (int j = 0; j < x.Length; j++)
                {
                    if (x_sort[i] == x[j] && x_rank[j] == -1)
                    {
                        x_rank[j] = i;
                        x_sort[i] = -10000;
                    }

                    if (y_sort[i] == y[j] && y_rank[j] == -1)
                    {
                        y_rank[j] = i;
                        y_sort[i] = -10000;
                    }
                }
            }

            /*
            for (int i = 0; i < x_rank.Length; i++)
            {
                dPowTotal += (x_rank[i] - y_rank[i]) * (x_rank[i] - y_rank[i]);
            }

            return 1 - dPowTotal * 6 / (x_rank.Length * (x_rank.Length * x_rank.Length - 1));
             */
            return GetPearsonCorrelation(x_rank, y_rank);
        }

        /*
        public double GetCorrelation(double[] x, double[] y)
        {
            //int startIdx = x.Length - (x.Length/4);
            int startIdx = 800;
            double sum_x = 0;
            double sum_x_sqrt = 0;
            for (int i = startIdx; i < x.Length; i++)
            {
                sum_x += x[i];
                sum_x_sqrt += x[i]*x[i];
            }

            double sum_y = 0;
            double sum_y_sqrt = 0;
            for (int i = startIdx; i < x.Length; i++)
            {
                sum_y += y[i];
                sum_y_sqrt += y[i]*y[i];
            }

            double sum_xy = 0;
            for (int i = startIdx; i < x.Length; i++)
                sum_xy += x[i] * y[i];

            double upper = (x.Length - startIdx) * sum_xy - sum_x * sum_y;
            double lower = Math.Sqrt(Math.Abs((x.Length - startIdx) * sum_x_sqrt - sum_x * sum_x)) * Math.Sqrt(Math.Abs((x.Length - startIdx) * sum_y_sqrt - sum_y * sum_y));

            Console.WriteLine("Upper: " + upper + " Lower: " + lower);

            return upper / lower;
        }
        */

        public double GetError(double currError, INeuralDataSet idealSet)
        {
            //if (idealSet == null)
                return currError;

            //return StartTestProcedure(idealSet);
        }

        public double[][] StartTestProcedure(INeuralDataSet testingSet)
        {
            
            IEnumerator<INeuralDataPair> testingSetIt = testingSet.GetEnumerator();
            int count = 0;
            while (testingSetIt.MoveNext())
                count++;

            double[][] result;
            result = new double[2][];
            result[0] = new double[count];
            result[1] = new double[count];

            count = 0;
            testingSetIt = testingSet.GetEnumerator();
            INeuralDataPair dataPair = null;
            while (testingSetIt.MoveNext())
            {
                dataPair = testingSetIt.Current;
                INeuralData output = m_network.Compute(dataPair.Input);
                result[0][count] = dataPair.Ideal[0];
                result[1][count] = output.Data[0];
                //Console.WriteLine(output.Data[0]);//"Local error: " + dataPair.Ideal[0] + " " + output.Data[0] + " " + error + " " + ": " + Math.Abs((dataPair.Ideal[0] - output.Data[0]) / output.Data[0]));
                count++;
            }
            return result;
        }
    }
    /*
public void StartTesting(ref double[][,] TestVec, out  double[][] output,ref globalParam Param)
{
    int negative =  Param.Readout_Negative;
    output = new double[TestVec.Length][];
    double[,][] tempOutput = new double[TestVec.Length,Param.How_Many_Windows][];
    double[] tempVec = new double[this.InputSize];
		
    for (int i = 0; i < TestVec.Length; i++)
    {
        int Index_Window_Counter=0;

        for (int j = 0; j < TestVec[i].GetLength(1); j++) // Nuerons
        {
            if ( j < Param.LSM_AdjustmentTime) { continue;}
            if ( j != this.Windows_Index[Index_Window_Counter] ) {continue;}
				
            int counter = 0;
            for (int windowSize=0 ; windowSize < Param.ReadoutU_Window_Size ; windowSize++ ) {
                for (int tempI = 0; tempI < TestVec[i].GetLength(0); tempI++) //time
                {
                    if (TestVec[i][tempI, j+windowSize] >= (Param.Neuron_Threshold)){ tempVec[counter] = 1;}
                    else {tempVec[counter] = 0;}
                    counter++;
                }
            }
            tempOutput[i,Index_Window_Counter]= tempVec;
            tempVec = new double[tempVec.Length];
            if ( Index_Window_Counter<Windows_Index.Length-1) {Index_Window_Counter++;}
        }
    }
		
    if (this.model==1){
        Console.WriteLine("Testing Perceptron.....");
        for(int OutPutLint=0 ; OutPutLint < tempOutput.GetLength(0) ; OutPutLint++){
				
            output[OutPutLint] = new double[Param.How_Many_Windows];
            for(int window = 0 ; window < Param.How_Many_Windows ; window++){
                output[OutPutLint][window] = Per[window].Compute(tempOutput[OutPutLint,window]);
            }
        }
    }else{
        Console.WriteLine("Testing.....");
        for(int OutPutLint=0 ; OutPutLint < tempOutput.GetLength(0) ; OutPutLint++){
				
            if (this.numOfReadOutUnits==1)
                output[OutPutLint] = new double[Param.How_Many_Windows];
            else
                output[OutPutLint] = new double[this.numOfReadOutUnits];
            for (int i = 0; i < this.OutputSize; i++) {
                output[OutPutLint][i] =  0;
            }
            for(int window = 0 ; window < Param.How_Many_Windows ; window++){
                int readOutNumber=window;
                if (this.numOfReadOutUnits==1) readOutNumber=0;
                INeuralData Netoutput = this.network[readOutNumber].Compute(new BasicNeuralData(tempOutput[OutPutLint,window]));
                for (int i = 0; i < this.OutputSize; i++) {
                    output[OutPutLint][window] +=  Netoutput.Data[i] * this.Window_Accuracy[window];
                }
            }
        }
    }
}//---------------------------------------------------------------------------------
     */
}
