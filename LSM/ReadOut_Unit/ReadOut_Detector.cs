using System;
using System.Collections.Generic;

using NN_Pr;

using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Activation;
using Encog.Neural.Data.Basic;
using Encog.Neural.NeuralData;
using Encog.Neural.NeuralData.Bipolar;
using Encog.Neural.Networks.Training;
using Encog.Neural.Data;
using Encog.Neural.Networks.Training.Simple;
using Encog.Neural.Networks.Training.Propagation.Gradient;
using Encog.Neural.Networks.Training.Propagation.Back;
using Encog.Neural.Networks.Training.Propagation.Manhattan;
using Encog.Neural.Networks.Training.Propagation.Resilient;
using Encog.Neural.Networks.Training.Propagation.SCG;
using Encog.Neural.Networks.Training.Strategy;
using Encog.Util.Banchmark;
using Encog.Util.Logging;

//

public class ReadOut_Detector
{
	
	public int NetSize,InputSize,OutputSize;
	private int model,mode,numOfReadOutUnits;
	private int[] Windows_Index;
	public double[] Window_Accuracy;

	Perceptron[] Per;
	
	//07-10-2009
	BasicNetwork[] network;
	//

	public ReadOut_Detector(int inputSize, int hiddenSize, int outputSize,ref globalParam Param)
	{
		//---------------------------------------------------------------------------------
		//   Init Backpropagation Network
		//---------------------------------------------------------------------------------
		Console.WriteLine("Inisilaize Readout Neuronal Network ");
		this.model = Param.ReadOut_Unit;
		this.mode = Param.Readout_Unit_Mode;
		
		if (mode==1){
			this.InputSize=inputSize;
		}else if (mode==2){
			this.InputSize=inputSize*Param.ReadoutU_Window_Size;
		}
		
		Utils_Functions.randomIntArr rnd = new Utils_Functions.randomIntArr(Convert.ToInt32(System.DateTime.UtcNow.Ticks%int.MaxValue));
		
		this.Windows_Index = new int[Param.How_Many_Windows];
		
		// Create the windows indexes in jumps of Disctance and make jitter on indies
		int counter=Param.LSM_AdjustmentTime;
		for (int i = 0; i < this.Windows_Index.Length; i++) {
			this.Windows_Index[i] = ((i==0)||(i==this.Windows_Index.Length-1))? counter : counter+rnd.RandomNum.Next(-5,5);
			counter+=Param.ReadoutU_Disctance_Between_Windows+Param.ReadoutU_Window_Size;
		}
		
		this.OutputSize=outputSize;
		this.NetSize=InputSize+hiddenSize+outputSize;
		
		if (Param.numOfReadOutUnits==1)
			this.numOfReadOutUnits=1;
		else
			this.numOfReadOutUnits = Param.How_Many_Windows;
		
		if (model==1) {
			this.Per = new Perceptron[this.numOfReadOutUnits];
		}else{
			this.network = new BasicNetwork[this.numOfReadOutUnits];
		}

		if (hiddenSize==0) hiddenSize=1;
		
		
		for (int i = 0 ; i <this.numOfReadOutUnits  ; i++)
		{
			if (model==1) {
				this.Per[i] = new Perceptron(this.InputSize);
			}else{
				this.network[i] = new BasicNetwork();
			}
			if (model==2) {
				this.network[i].AddLayer(new BasicLayer(new ActivationLinear(), true, this.InputSize));
				this.network[i].AddLayer(new BasicLayer(new ActivationTANH(), true, this.OutputSize));
			}
			if (model>2){
				this.network[i].AddLayer(new BasicLayer(new ActivationTANH(), true, this.InputSize));
				this.network[i].AddLayer(new BasicLayer(new ActivationTANH(), true, hiddenSize));
				this.network[i].AddLayer(new BasicLayer(new ActivationTANH(), true, this.OutputSize));
			}
			if (model>1) {
				this.network[i].Structure.FinalizeStructure();
				this.network[i].Reset();
			}
		}
		
		//
		
		
	}//--------------------------------------------------------------------
	
	public ReadOut_Detector(int inputSize, int hiddenSize, int outputSize, int model)
	{
		//---------------------------------------------------------------------------------
		//   Init Backpropagation Network
		//---------------------------------------------------------------------------------
		Console.WriteLine("Inisilaize Readout Neuronal Network ");
		
		this.OutputSize=outputSize;
		this.InputSize = inputSize;
		this.NetSize=InputSize+hiddenSize+outputSize;
		this.model = model;
		
		if (hiddenSize==0) hiddenSize=1;
		
		this.Per = new Perceptron[1];
		this.network = new BasicNetwork[1];
		for (int i = 0 ; i < 1  ; i++)
		{
			if (model==1) {
				this.Per[i] = new Perceptron(this.InputSize);
			}else{
				this.network[i] = new BasicNetwork();
			}
			if (model==2) {
				this.network[i].AddLayer(new BasicLayer(new ActivationLinear(), true, this.InputSize));
				this.network[i].AddLayer(new BasicLayer(new ActivationTANH(), true, this.OutputSize));
			}
			if (model>2){
				this.network[i].AddLayer(new BasicLayer(new ActivationTANH(), true, this.InputSize));
				this.network[i].AddLayer(new BasicLayer(new ActivationTANH(), true, hiddenSize));
				this.network[i].AddLayer(new BasicLayer(new ActivationTANH(), true, this.OutputSize));
			}
			if (model>1) {
				this.network[i].Structure.FinalizeStructure();
				this.network[i].Reset();
			}
		}
	}//--------------------------------------------------------------------

	public void StratTrain(ref double[][,] LearnVec,ref globalParam.Data[] LearnData,
	                       ref double LastReturnError,ref globalParam Param)
	{
		int size = LearnData.Length;
		double[] targetLearnData = new double[size];
		for( int i = 0 ; i < size ; i++){	targetLearnData[i] = LearnData[i].Target[0];	}
		this.StratTrain(ref LearnVec,ref targetLearnData,ref LastReturnError ,ref Param);
	}

	public void StratTrain(ref double[][,] LearnVec,ref double[] TargetVec,
	                       ref double LastReturnError,ref globalParam Param)
	{
		double[][][] trainingSet;
		double[][][] trainingResult;
		LastReturnError = 0 ;
		
		trainingSet = new double[Param.How_Many_Windows][][];
		trainingResult = new double[Param.How_Many_Windows][][];
		
		for (int window = 0 ;  window < Param.How_Many_Windows ; window++ ){
			trainingSet[window] = new double[LearnVec.Length][];
			trainingResult[window] = new double[LearnVec.Length][];
		}
		
		double[] tempVec  = new double[this.InputSize];;
		
		for (int i = 0; i < LearnVec.Length; i++)
		{
			int Index_Window_Counter=0;

			for (int j = 0; j < LearnVec[i].GetLength(1); j++) // Nuerons
			{
				if ( j < Param.LSM_AdjustmentTime) { continue;}
				if ( j != this.Windows_Index[Index_Window_Counter] ) {continue;}
				
				int counter = 0;
				// convert window to vector
				for (int windowSize=0 ; windowSize < Param.ReadoutU_Window_Size ; windowSize++ ) {
					for (int tempI = 0; tempI < LearnVec[i].GetLength(0); tempI++) //time
					{
						if (LearnVec[i][tempI, j+windowSize] >= (Param.Neuron_Threshold)){ tempVec[counter] = 1;}
						else {tempVec[counter] = 0;}
						counter++;
					}
				}
				trainingSet[Index_Window_Counter][i]= tempVec;
				trainingResult[Index_Window_Counter][i]=new double[] {TargetVec[i]};
				tempVec = new double[tempVec.Length];
				if ( Index_Window_Counter<Windows_Index.Length-1) {Index_Window_Counter++;}

			}
		}

		if (this.model==1){
			Console.WriteLine("Perceptron Training.....");
			List<Pattern>[] patterns;
			patterns = new List<Pattern>[Param.How_Many_Windows];
			for(int i=0; i<trainingSet.Length ; i++){
				patterns[i]= new List<Pattern>();
				for (int window = 0 ; window < this.numOfReadOutUnits ; window++ ) {
					patterns[window].Add(new Pattern(trainingSet[window][i], trainingResult[window][i]));
				}
			}
			double error=0;
			for (int window = 0 ; window < Param.How_Many_Windows ; window++ ) {
				error+= Per[window].Train(patterns[window]);
			}
			Console.WriteLine("Finish... Error = {0}",(error/(trainingSet.Length+Param.How_Many_Windows)));
		}
		
		if (this.model>1) {
			//07-10-2009
			Console.WriteLine("Start Training ");
			INeuralDataSet GtrainingSet;
			ITrain train;
			
			this.Window_Accuracy = new double[Param.How_Many_Windows];
			
			// train the neural network
			for (int i = 0 ; i < Param.How_Many_Windows ; i++)
			{
				int readOutNumber=i;
				if (this.numOfReadOutUnits==1) readOutNumber=0;
				
				string modelName;
				GtrainingSet = new BasicNeuralDataSet(trainingSet[i], trainingResult[i]);
				switch (this.model) {
						
					case 2:
						train = new TrainAdaline(network[readOutNumber], GtrainingSet, 0.001);
						modelName="Adaline";
						break;
						
					case 3:
						train = new Backpropagation(network[readOutNumber], GtrainingSet);
						modelName="Backpropagation";
						break;
						
					case 4:
						train = new ManhattanPropagation(network[readOutNumber], GtrainingSet,0.1);
						train.AddStrategy(new SmartLearningRate());
						modelName="Manhattan Backpropagation";
						break;
						
					case 5:
						train = new ResilientPropagation(network[readOutNumber], GtrainingSet);
						modelName="Resilient Backpropagation";
						break;
						
					case 6:
						train = new ScaledConjugateGradient(network[readOutNumber], GtrainingSet);
						modelName="Scaled Conjugate Gradient";
						break;
						
					default:
						train = new TrainAdaline(network[readOutNumber], GtrainingSet, 0.01);
						modelName="Adaline";
						break;
				}
				
				int epoch = 1;
				do
				{
					train.Iteration();
					epoch++;
				} while ((epoch < Param.ReadoutU_epoch) && (train.Error > Param.Readout_Max_Error));
				Console.WriteLine("window "+i.ToString()+" " +modelName+" Epoch #" + epoch.ToString() + " Error:" + train.Error.ToString());
				this.Window_Accuracy[i] = (train.Error>1)? 0: 1-train.Error;
				LastReturnError += train.Error;
			}
			LastReturnError = LastReturnError /Param.How_Many_Windows;
		}
		
	}//--------------------------------------------------------------------


	
	public void StratTrain(ref globalParam Param,out double[] LastReturnError)
	{
		LastReturnError = new double[Param.LearnData.Length];
		
		if (this.model==1){
			Per = new Perceptron[1];
			Console.WriteLine("Perceptron Training.....");
			List<Pattern>[] patterns;
			patterns = new List<Pattern>[Param.LearnData.Length];
			for(int i=0; i<Param.LearnData.Length ; i++){
				patterns[i]= new List<Pattern>();
				patterns[i].Add(new Pattern(Param.LearnData[i].Input[0], new double[]{Param.LearnData[i].Target[0]}));
			}
			double error=0;
			for (int p = 0 ; p < patterns.Length ; p ++ ) {
				error+= Per[0].Train(patterns[p]);
			}
			Console.WriteLine("Finish... Error = {0}",(error/patterns.Length));
		}
		
		if (this.model>1) {
			//07-10-2009
			Console.WriteLine("Start Training ");
			INeuralDataSet GtrainingSet;
			ITrain train;
			
			// train the neural network
			for (int i = 0 ; i <Param.LearnData.Length ; i++)
			{
				string modelName;
				double[][] temp = new double[1][];
				temp[0] = new double[]{Param.LearnData[i].Target[0]};
				double[][] temp2 = new double[1][];
				temp2[0] = new double[Param.LearnData[i].Input.Length];
				for (int t = 0; t < Param.LearnData[i].Input.Length ; t++) {
					temp2[0][t] = Param.LearnData[i].Input[t][0];
				}
				
				
				GtrainingSet = new BasicNeuralDataSet(temp2, temp);
				switch (this.model) {
						
					case 2:
						train = new TrainAdaline(network[0], GtrainingSet, 0.001);
						modelName="Adaline";
						break;
						
					case 3:
						train = new Backpropagation(network[0], GtrainingSet);
						modelName="Backpropagation";
						break;
						
					case 4:
						train = new ManhattanPropagation(network[0], GtrainingSet,0.1);
						train.AddStrategy(new SmartLearningRate());
						modelName="Manhattan Backpropagation";
						break;
						
					case 5:
						train = new ResilientPropagation(network[0], GtrainingSet);
						modelName="Resilient Backpropagation";
						break;
						
					case 6:
						train = new ScaledConjugateGradient(network[0], GtrainingSet);
						modelName="Scaled Conjugate Gradient";
						break;
						
					default:
						train = new TrainAdaline(network[0], GtrainingSet, 0.01);
						modelName="Adaline";
						break;
				}
				
				int epoch = 1;
				do
				{
					train.Iteration();
					epoch++;
				} while ((epoch < Param.ReadoutU_epoch) && (train.Error > Param.Readout_Max_Error));
				Console.WriteLine("DataSet "+i.ToString()+" " +modelName+" Epoch #" + epoch.ToString() + " Error:" + train.Error.ToString());
				LastReturnError[i] = train.Error;
			}
		}
		
	}//--------------------------------------------------------------------
	
	public void StratTesting(ref double[][,] TestVec, out  double[][] output,ref globalParam Param)
	{
		int negative =  Param.Readout_Negative;
		output = new double[TestVec.Length][];
		double[,][] tempOutput = new double[TestVec.Length,Param.How_Many_Windows][];
		double[] tempVec = new double[this.InputSize];
		
		for (int i = 0; i < TestVec.Length; i++)
		{
			int Index_Window_Counter=0;

			for (int j = 0; j < TestVec[i].GetLength(1); j++) // Nuerons
			{
				if ( j < Param.LSM_AdjustmentTime) { continue;}
				if ( j != this.Windows_Index[Index_Window_Counter] ) {continue;}
				
				int counter = 0;
				for (int windowSize=0 ; windowSize < Param.ReadoutU_Window_Size ; windowSize++ ) {
					for (int tempI = 0; tempI < TestVec[i].GetLength(0); tempI++) //time
					{
						if (TestVec[i][tempI, j+windowSize] >= (Param.Neuron_Threshold)){ tempVec[counter] = 1;}
						else {tempVec[counter] = 0;}
						counter++;
					}
				}
				tempOutput[i,Index_Window_Counter]= tempVec;
				tempVec = new double[tempVec.Length];
				if ( Index_Window_Counter<Windows_Index.Length-1) {Index_Window_Counter++;}
			}
		}
		
		if (this.model==1){
			Console.WriteLine("Testing Perceptron.....");
			for(int OutPutLint=0 ; OutPutLint < tempOutput.GetLength(0) ; OutPutLint++){
				
				output[OutPutLint] = new double[Param.How_Many_Windows];
				for(int window = 0 ; window < Param.How_Many_Windows ; window++){
					output[OutPutLint][window] = Per[window].Compute(tempOutput[OutPutLint,window]);
				}
			}
		}else{
			Console.WriteLine("Testing.....");
			for(int OutPutLint=0 ; OutPutLint < tempOutput.GetLength(0) ; OutPutLint++){
				
				if (this.numOfReadOutUnits==1)
					output[OutPutLint] = new double[Param.How_Many_Windows];
				else
					output[OutPutLint] = new double[this.numOfReadOutUnits];
				for (int i = 0; i < this.OutputSize; i++) {
					output[OutPutLint][i] =  0;
				}
				for(int window = 0 ; window < Param.How_Many_Windows ; window++){
					int readOutNumber=window;
					if (this.numOfReadOutUnits==1) readOutNumber=0;
					INeuralData Netoutput = this.network[readOutNumber].Compute(new BasicNeuralData(tempOutput[OutPutLint,window]));
					for (int i = 0; i < this.OutputSize; i++) {
						output[OutPutLint][window] +=  Netoutput.Data[i] * this.Window_Accuracy[window];
					}
				}
			}
		}
	}//---------------------------------------------------------------------------------
	
	public void StratTesting(ref globalParam Param, out  double[] output)
	{
		output = new double[Param.TestData.Length];
		
		if (this.model==1){
			Console.WriteLine("Testing Perceptron.....");
			for(int window = 0 ; window < Param.TestData.Length ; window++){
				output[window] = Per[window].Compute(Param.TestData[window].Input[0]);
			}
		}else{
			Console.WriteLine("Testing.....");
			for(int OutPutLint=0 ; OutPutLint < Param.TestData.Length ; OutPutLint++){
				double[] compute = new double[Param.TestData[OutPutLint].Input.Length];
				for (int i = 0; i < Param.TestData[OutPutLint].Input.Length; i++) {
					compute[i] = Param.TestData[OutPutLint].Input[i][0];
				}
				INeuralData Netoutput = this.network[0].Compute(new BasicNeuralData(compute));
				output[OutPutLint]=  Netoutput.Data[0];
			}
		}
	}//---------------------------------------------------------------------------------
}
