﻿/*
 * Created by SharpDevelop.
 * User: Hananel
 * Date: 02/10/2009
 * Time: 12:48
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;


namespace NN_Pr
{
	/// <summary>
	/// Description of Perceptron.
	/// </summary>
	
	public class Perceptron
	{
		Random r = new Random();
		double[] weights;
		public double learningRate,globalError;
		public int maxIteration;
		
		public Perceptron(int Inputsize){
			System.Console.WriteLine("Perceptron Inizilaize.... ");
			this.weights = new double[Inputsize]; for(int i=0; i<Inputsize; i++){	this.weights[i]=r.NextDouble();  }
			this.globalError=0;
			this.learningRate=0.05;
			this.maxIteration=1000;
			System.Console.WriteLine("Done.");
		}
		
		public double Train(List<Pattern> patterns){
			System.Console.WriteLine("Perceptron Training.... ");
			double iterError = 0;
			int iteration = 0;
			do{
				iterError = 0;
				for (int p = 0; p < patterns.Count; p++)
				{
					double[] inputs = patterns[p].Inputs;
					double outputs = patterns[p].Outputs[0];
					// Calculate error.
					double localError = outputs - this.Compute(inputs);
					if (localError != 0)
					{
						// Update weights.
						for (int i = 0; i < this.weights.Length; i++)
						{
							this.weights[i] += this.learningRate * localError * inputs[i];
						}
					}
					// Convert error to absolute value.
					iterError += Math.Abs(localError);
				}

				Console.WriteLine("Iteration {0}\tError {1}", iteration, iterError);
				iteration++;
				
			} while ((iterError != this.globalError)&&(iteration!=this.maxIteration));
			
			System.Console.WriteLine("Finish Training ");
			return iterError;
		}
		
		public double Sigmoid(double x)
		{
			return 1 / (1 + Math.Exp(-x));
		}
		
		public int Function(double x)
		{
			return (x >= 0) ? 1 : -1;
		}

		public double Compute(double[] input){
			
			double sum=0;
			for ( int i=0; i<input.Length ; i++) {
				sum += input[i]*this.weights[i];
			}
			//return this.Sigmoid(sum);
			return this.Function(sum);
		}
	}
	
	
	public class Pattern
	{
		private double[] inputs;
		private double[] outputs;
		
		public Pattern(double[] inputs,double[] outputs)
		{
			this.inputs=inputs;
			this.outputs=outputs;
		}
		
		public int MaxOutput
		{
			get
			{
				int item = -1;
				double max = double.MinValue;
				for (int i = 0; i < Outputs.Length; i++)
				{
					if (Outputs[i] > max)
					{
						max = Outputs[i];
						item = i;
					}
				}
				return item;
			}
		}
		
		public double[] Inputs
		{
			get { return inputs; }
		}
		
		public double[] Outputs
		{
			get { return outputs; }
		}
	}
}
