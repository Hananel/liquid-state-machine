using System;
using Utils_Functions;

namespace Neurons
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class Neuron
	{
		//----------------------------------------------------------------
		//                    init ver
		//----------------------------------------------------------------
		NeuronArch.Unit Nunit;
		public double ExternalIntput;
		public int firedBefore_Iteration;
		int[] suspicious;
		public int posiORneg;
		public bool candidateSTDP;
		int outputHz;
		
		Neuron[] OutputList;
		int[] weightInex;

		double[] weightsInput;
		double[] iniWeightsInput;
		int[] delaysInput;
		public Neuron[] inputFromNeuronID;
		int[] inputHz;

		int[] delayQu;
		double[] voltInputQu;
		
		public int name;
		public int mode;
		public int avgCounter;
		public Int64 firing_count;
		
		//----------------------------------------------------------------

		public Neuron(int model, int name,ref globalParam Param)
		{
			// model: 1=LIF , 2=Izhikevich , 3=LIF model 2 , 4=McCulloch_Pitts
			switch (model)
			{
					case 1: Nunit = new NeuronArch.LIFNeuron(ref Param); 		break;
					case 2: Nunit = new NeuronArch.IzakNeuron(ref Param); 		break;
					case 3: Nunit = new NeuronArch.LIFmodle2(ref Param); 		break;
					case 4: Nunit = new NeuronArch.McCulloch_Pitts(ref Param); 	break;
			}
			
			this.OutputList = new Neuron[0];
			this.weightInex = new int[0];
			this.weightsInput = new Double[0];
			this.iniWeightsInput = new Double[0];
			this.delaysInput = new int[0];
			this.inputFromNeuronID = new Neuron[0];
			this.suspicious = new int[0];
			this.inputHz = new int[0];
			this.name = name;
			this.candidateSTDP=false;
			
			this.reset(ref Param);
			
		}//----------------------------------------------------------------
		
		
		public void reset(ref globalParam Param)
		{
			this.delayQu = new int[0];
			this.voltInputQu = new double[0];
			this.ExternalIntput = 0;
			this.mode = 1;
			this.weightsInput = (double[]) iniWeightsInput.Clone();
			this.firedBefore_Iteration=0;
			this.candidateSTDP=false;
			for ( int i = 0 ; i < this.inputFromNeuronID.Length ; i++ ) {
				this.suspicious[i]=0;
				this.inputHz[i]=0;
			}
			this.outputHz = 0;
			Nunit.reset(ref Param);
			avgCounter = 0 ;
			firing_count = 0;
		}//----------------------------------------------------------------
		
		
		public void step(int runningTime,ref globalParam Param)
		{
			double internalInput=0,nu;
			switch (this.mode)
			{
				case 1:  // Normal
					internalInput = this.popInputQ();
					break;
					
				case 2: // Noise Generator
					this.ExternalIntput=Param.Neuron_Spike;
					// and continue Normaly to case 1
					goto case 1;

				case 3: // Dead unit
					nu=this.popInputQ();
					this.ExternalIntput = 0;
					internalInput = 0;
					this.Nunit.setV(-65);
					break;
			}
			
			this.firedBefore_Iteration--;
			double Vout = (Nunit.step(internalInput, this.ExternalIntput));
			int SorV=Param.Neuron_Spike_or_Volt;
			
			if ((Vout > 0)&&(this.mode!=3)){
				this.firedBefore_Iteration=0;
				this.outputHz++;
				this.firing_count++;
				// Fire to whom neuron is conneced.
				if (SorV == 1) this.OutputToAllConnectedUnits(Param.Neuron_Spike);
				else this.OutputToAllConnectedUnits(Vout);
				
				if ( this.ExternalIntput>0) {this.candidateSTDP=true; }
				
			}else{
				this.candidateSTDP=false;
			}
			
			if ((this.outputHz>0)&&(runningTime > Param.LSM_1sec_interval)) this.outputHz--;
			
			this.mode = 1;
			this.ExternalIntput = 0;
		}//----------------------------------------------------------------
		
		
		public void LTP_D(int runningTime,ref globalParam Param)
			// This func check every afferent of the neuron for its frequences
			// if the fireing frequency of the afferents is:
			
			// 3Hz  --> LTD --> Depress this synapse
			// 50Hz --> LTP --> Potentiate this synapse
			// 3Hz < X < 50hz  -->  don't do a thing
			
			// definition : frequency of 1 Hz is equal to one cycle per second.
		{
			if ( this.posiORneg==1) {// this neuron is Positive

				// check the Hz of all affrents
				for (int i=0 ; i<this.inputFromNeuronID.Length; i++) {
					if ((this.inputHz[i]>0)&&(runningTime > Param.LSM_1sec_interval)) this.inputHz[i]--;
					if ((this.inputFromNeuronID[i].candidateSTDP==false)&&(this.inputFromNeuronID[i].firedBefore_Iteration==0))	this.inputHz[i]++;
					if (runningTime % (Param.LSM_1sec_interval) == 0){
						if ( (this.inputHz[i]>49)&&(this.inputHz[i]<51) )
							this.weightsInput[i]+= 0.05*(50/this.inputHz[i])*this.weightsInput[i];
						if ( (this.inputHz[i]>2)&&(this.inputHz[i]<4) )
							this.weightsInput[i]-= 0.05*(3/this.inputHz[i])*this.weightsInput[i];
					}
				}
			}
			else {// this neuron is Negative
			}
		}//----------------------------------------------------------------
		
		
		public void STDP(ref globalParam Param)
		{
			int LTPwindow = Param.Neuron_LTP_window;
			int LTDwindow = Param.Neuron_LTD_window;
			if (LTPwindow+LTDwindow>0){
				
				double aP,aN;
				aP=Param.Neuron_LTP;
				aN=Param.Neuron_LTD;
				if ( this.posiORneg==1) { // this neuron is Positive
					
					if ((this.candidateSTDP==true)&&(this.firedBefore_Iteration==0)){
						
						// if this Neuron fire because External Input then check who else fire and LTP
						for (int i=0 ; i<this.inputFromNeuronID.Length; i++) {
							
							int fireIteration = -1 * this.inputFromNeuronID[i].firedBefore_Iteration;
							// "fireIteration" can be Zero OR negative namber! If is smaller then the LTP its mean
							// that it didnt fire and it could fire. Enter to the suspicious  list!
							if ( fireIteration<=LTPwindow )  { this.suspicious[i] = LTDwindow;   continue;  }
							
							//else... continue....Start LTP
							this.suspicious[i] = 0;
							
							double deltaWeigth = aP *((LTPwindow-fireIteration)/LTPwindow);
							//deltaWeigth=  aP * Math.Exp((-1*fireIteration)/LTPwindow);
							
							if (this.weightsInput[i]<0){ this.weightsInput[i]-=deltaWeigth;}
							else{ this.weightsInput[i]+=deltaWeigth; }
						}
					}else{  // is the neuron didnt fire , check the Suspicuous list
						for (int i=0 ; i<this.inputFromNeuronID.Length; i++) {
							if ( this.suspicious[i]<=0 ) { continue;}

							if ( this.inputFromNeuronID[i].firedBefore_Iteration==0) {
								
								double deltaWeigth =  aN *((this.suspicious[i])/LTDwindow);
								
								if (this.weightsInput[i]<0){ this.weightsInput[i]+=deltaWeigth;}
								else{ this.weightsInput[i]-=deltaWeigth; }
								this.suspicious[i]=0;
								
							}else{ this.suspicious[i]--; }
						}
					}
				}else { // this Neuron is negative!!
				}
			}
			
		}//----------------------------------------------------------------
		
		public double returnV()
		{
			return (this.Nunit.returnV());
		}//----------------------------------------------------------------
		
		public Neuron returnRef()
		{
			return (this);
		}//----------------------------------------------------------------
		
		public bool returnOutput()
		{
			return (this.Nunit.returnOutput());
		}//----------------------------------------------------------------
		
		public void OutputToAllConnectedUnits(double volts)
		{
			Neuron source = this;
			for (int i = 0; i < this.OutputList.Length; i++)
				(this.OutputList[i]).EnterToInputQu(ref source, volts,this.weightInex[i]);
		}//----------------------------------------------------------------
		
		public void addNueronToOutputList(ref Neuron source,int index,ref globalParam Param)
		{
			int ArraySize = this.OutputList.Length;
			int ArraySizeP = ArraySize+1;
			
			Array.Resize(ref this.OutputList,ArraySizeP);
			Array.Resize(ref this.weightInex,ArraySizeP);
			
			this.OutputList[ArraySize] = source;
			this.weightInex[ArraySize] = index;
			
		}//----------------------------------------------------------------
		
		public int addNueronToInputList(ref Neuron source, double weight, int delay)
		{
			int ArraySize = this.inputFromNeuronID.Length;
			int ArraySizeP = ArraySize+1;
			
			Array.Resize(ref inputFromNeuronID,ArraySizeP);
			Array.Resize(ref iniWeightsInput,ArraySizeP);
			Array.Resize(ref delaysInput,ArraySizeP);
			Array.Resize(ref suspicious,ArraySizeP);
			Array.Resize(ref inputHz,ArraySizeP);
			
			this.inputFromNeuronID[ArraySize] = source;
			this.iniWeightsInput[ArraySize] = weight;
			this.delaysInput[ArraySize] = delay;
			this.suspicious[ArraySize] = 0;
			this.inputHz[ArraySize] = 0;
			
			return (ArraySize);
		}//----------------------------------------------------------------
		
		public void EnterToInputQu(ref Neuron source, double volts,int index)
		{
			int flag=0;
			int size = this.delayQu.Length;
			if (size>0){
				int temp = this.delaysInput[index];
				for (int i = 0 ; i < size ; i++)
					if (this.delayQu[i] == temp) {
					flag=1;
					this.voltInputQu[i] += this.weightsInput[index] * volts;
					break;
				}
			}
			if(flag==0){
				int sizePP = size+1;
				
				Array.Resize(ref this.delayQu,sizePP);
				Array.Resize(ref this.voltInputQu,sizePP);
				
				this.delayQu[size] = this.delaysInput[index];
				this.voltInputQu[size] = this.weightsInput[index] * volts;
			}
			
		}//----------------------------------------------------------------
		
		
		public void decInputQ(){
			for (int i = 0; i < this.delayQu.Length; i++) this.delayQu[i]--;
		}//----------------------------------------------------------------
		
		public double popInputQ()
		{
			double input = 0;
			int size = this.delayQu.Length;
			if (size > 0)
			{
				int logflag,inputflag; logflag=inputflag= 0;
				for (int i = 0; i < size; i++)
					if (this.delayQu[i] <= 0)
				{
					input += this.voltInputQu[i];
					inputflag++;
				}
				
				if (inputflag > 0 )
				{
					int[] olddelay = (int[]) this.delayQu.Clone();
					double[] oldvinput = (double[]) this.voltInputQu.Clone();

					int temp = size - inputflag;
					this.delayQu = new int[temp];
					this.voltInputQu = new double[temp];
					
					int mone = 0;
					for (int i = 0; i < size; i++)
					{
						if (olddelay[i] <= 0) continue;
						this.delayQu[mone] = olddelay[i];
						this.voltInputQu[mone] = oldvinput[i];
						mone++;
					}
				}
			}
			
			return input;
		}//----------------------------------------------------------------

		public void setThreshold(double NewT){
			this.Nunit.setThreshold(NewT);
		}
		
	} // End Class
}
