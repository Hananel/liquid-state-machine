
using System;

namespace NeuronArch
{

	public class LIFmodle2 : NeuronArch.Unit
	{
		//----------------------------------------------------------------
		//                    init ver
		//----------------------------------------------------------------
		double initV;
		double refactoryV;
		public double V;
		double therashold,initTherashold;
		double inputLeft;
		public double Output;
		double previousInput;

		double decayRate;
		double inputRate;
		double refactoryRate;
		
		double energy;
		double energy_fact;
		//----------------------------------------------------------------

		public LIFmodle2(ref globalParam Param)
		{
			this.reset(ref Param);
		}

		//----------------------------------------------------------------
		public void reset(ref globalParam Param)
		{
			this.initV = Param.initV;
			this.V = initV;
			this.refactoryV = this.initV - 15;
			//this.initTherashold = Param.Neuron_Threshold;
			this.therashold = this.initTherashold;
			this.Output = 0;
			this.inputLeft = 0;
			this.previousInput = 0;

			this.decayRate = Param.decayFactor;
			this.inputRate = 1 - this.decayRate;
			this.refactoryRate = 0.5;
			
			this.energy = 1; // 100%
			this.energy_fact=0;
		}


		//----------------------------------------------------------------
		public double step(double InternalInput, double ExternalIntput)
		{
			this.Output = 0;

			if (this.V >= this.therashold) {// Spike
				this.Output = this.V+this.inputLeft;
				this.V = this.refactoryV;
				this.energy-=this.energy_fact/100;
				this.energy_fact+=2;
				if ( this.energy<=this.refactoryRate) {this.energy=this.refactoryRate;}
				
			}else if (this.V >= this.initV) {// Input + Decay
				double tempInput=InternalInput+ExternalIntput;
				
				if ((tempInput+this.V)>this.therashold) { 
					this.V = this.V + tempInput;  
				}else if ((tempInput+this.inputLeft>=0.1)||(tempInput+this.inputLeft<=-0.1)) {
					//if the New input is bigger then the previous, then increase the volt by the DELTA
					//if the New input is smaller then the previous, then decrease the volt by the DELTA
					//if the New input == the previous then there is no DELTA
					
					this.inputLeft+=(tempInput-this.previousInput);
					this.previousInput=(tempInput*0.5)+(this.previousInput*0.5);

					double tempV = this.V + this.inputLeft;
					this.V += (this.inputRate==0)? this.inputLeft:this.inputRate * this.inputLeft;
					this.inputLeft = tempV - this.V;
					
				}else{ // No input at all -> Decay!
					this.V = (this.V + this.inputLeft) + (this.decayRate * ((this.initV) - this.V));
					this.inputLeft=0;
					this.previousInput -=  this.decayRate * (this.previousInput);
					this.energy+=(1-this.energy)*this.refactoryRate;
					if (this.energy_fact>0) this.energy_fact-=2;
				}
				
			}else if (this.V < this.initV){ // Refactoring
				this.V -= this.energy * this.refactoryRate * (this.V - (this.initV+1));
				this.inputLeft = 0 ;
				this.previousInput = 0;
				if ( this.V>this.initV ) { this.V=this.initV; }
				
			}else{ 
				// Not supose to come here, If it does, there is a problem
				System.Console.WriteLine(" OOOOOPPPPPPPSSSSSSSS");
			}
			return (this.Output);
		}

		//----------------------------------------------------------------
		public double returnV()
		{
			return (this.V);
		}

		//----------------------------------------------------------------
		public bool returnOutput()
		{
			return (this.Output > this.therashold);
		}

		//----------------------------------------------------------------
		public void setV(double NewV)
		{
			this.V = NewV;
			this.inputLeft = 0 ;
			this.previousInput = 0;
		}
        //----------------------------------------------------------------
        
        public void setThreshold(double NewT)
        {
            this.initTherashold = NewT;
        }
        //----------------------------------------------------------------


	}
}
