﻿/*
 * Created by SharpDevelop.
 * User: hhazan01
 * Date: 07/01/2010
 * Time: 14:51
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using ZedGraph;


/// <summary>
/// Description of plotxy.
/// </summary>
public partial class plotxy : Form
{
	public PointPairList list1,list2;
	
	public plotxy()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		//
		// TODO: Add constructor code after the InitializeComponent() call.
		//
	}
	
	public void plotxy_Load( object sender, EventArgs e )
	{
		// get a reference to the GraphPane
		GraphPane myPane = zgc.GraphPane;

		// Set the Titles
		myPane.Title.Text = "Liquid Activity";
		myPane.YAxis.Title.Text = "Neurons";
		myPane.XAxis.Title.Text = "Time";
		
		// Generate a red curve with diamond
		// symbols, and "Porsche" in the legend
		LineItem myCurve = myPane.AddCurve( "Output", list1, Color.Red, SymbolType.Diamond );
		

		// Generate a blue curve with circle
		// symbols, and "Piper" in the legend
		LineItem myCurve2 = myPane.AddCurve( "Input", list2, Color.Blue, SymbolType.Diamond );
		
		myCurve.Symbol.Size = 3;
		myCurve2.Symbol.Size = 3;


		// Don't display the line (This makes a scatter plot)
		myCurve.Line.IsVisible = false;
		myCurve2.Line.IsVisible = false;
		// Hide the symbol outline
		myCurve.Symbol.Border.IsVisible = false;
		myCurve2.Symbol.Border.IsVisible = false;
		// Fill the symbol interior with color
		myCurve.Symbol.Fill = new Fill( Color.Firebrick );
		myCurve2.Symbol.Fill = new Fill( Color.Green );

		
		// Tell ZedGraph to refigure the
		// axes since the data have changed
		zgc.AxisChange();
		// Make sure the Graph gets redrawn
		zgc.Invalidate();

	}
	
	private void plotxy_Resize( object sender, EventArgs e )
	{
		SetSize();
	}

	private void SetSize()
	{
		zgc.Location = new Point( 10, 10 );
		// Leave a small margin around the outside of the control
		zgc.Size = new Size( this.ClientRectangle.Width - 20,this.ClientRectangle.Height - 20 );
	}
	
	
	public void loadData(ref double[,] matrix1,ref double[,] matrix2,ref globalParam Param){
		
		list1 = new PointPairList();
		list2 = new PointPairList();
		double Threshold = Param.Neuron_Threshold;

		// Make up some data arrays based on the Sine function
		
		for ( int i = 0; i < matrix1.GetLength(0) ; i++ )
			for (int t = 0 ; t < matrix1.GetLength(1) ; t++) {
			if (matrix1[i,t]>=Threshold) list1.Add(t,i);
		}
		
		for ( int i = 0; i < matrix2.GetLength(0) ; i++ )
			for (int t = 0 ; t < matrix2.GetLength(1) ; t++) {
			if (matrix2[i,t]>=Threshold) list2.Add(t,matrix1.GetLength(0)+i);
		}

		
	}
}

