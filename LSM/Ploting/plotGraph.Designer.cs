﻿/*
 * Created by SharpDevelop.
 * User: hhazan01
 * Date: 07/01/2010
 * Time: 14:51
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

partial class plotGraph
{
	/// <summary>
	/// Designer variable used to keep track of non-visual components.
	/// </summary>
	private System.ComponentModel.IContainer components = null;
	
	/// <summary>
	/// Disposes resources used by the form.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing)
	{
		if (disposing) {
			if (components != null) {
				components.Dispose();
			}
		}
		base.Dispose(disposing);
	}
	
	/// <summary>
	/// This method is required for Windows Forms designer support.
	/// Do not change the method contents inside the source code editor. The Forms designer might
	/// not be able to load this method if it was changed manually.
	/// </summary>
	private void InitializeComponent()
	{
		this.components = new System.ComponentModel.Container();
		this.zgc = new ZedGraph.ZedGraphControl();
		this.SuspendLayout();
		
		// 
		// plotGraph
		// 
		this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		this.Text = "plotGraph";
		this.Name = "plotGraph";
		this.Load += new System.EventHandler( this.plotGraph_Load );
		this.Resize += new System.EventHandler( this.plotGraph_Resize );
		this.ClientSize = new System.Drawing.Size( 523, 357 );
		this.Controls.Add( this.zgc);
		
	}
	
	private ZedGraph.ZedGraphControl zgc;
}

