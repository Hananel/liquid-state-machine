﻿/*
 * Created by SharpDevelop.
 * User: hhazan01
 * Date: 20/05/2010
 * Time: 13:13
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Utils_Functions
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class Manipulation_On_Inputs
	{
		
		public Manipulation_On_Inputs()
		{
		}//------------------------------------------------------------
		
		public void FrequencyFFTtoOnary(ref globalParam Param, ref globalParam.Data data, ref int[][] channle)
		{
			data.Input = new double[channle[0].Length][];
			int inputCounter=0;
			for (int input = 0; input < channle[0].Length; input++) {
				
				data.Input[inputCounter] = new double[channle.Length*Param.MaxLevel];
				int counter=0;
				for (int time  = 0; time  < channle.Length; time ++) {

					//convert to Unary representation
					string[] temp;
					if (channle[time][input]<=(Param.MaxLevel/2))
						temp = ToUnary(channle[time][input],Param.Neuron_Spike);
					else
						temp = new System.String[]{"0"};
					
					for (int i = 0; i < temp.Length; i++) {
						data.Input[inputCounter][counter] = Convert.ToDouble(temp[i]);
						counter++;
					}
					for (int i = temp.Length; i < Param.MaxLevel ; i++) {
						data.Input[inputCounter][counter] = 0;
						counter++;
					}
				}
				inputCounter++;
			}
		}
		//--------------------------------------------------------
		
		
		public void FrequencyFFTtoDirectInput(ref globalParam Param, ref globalParam.Data data, ref int[][] channle)
		{
			data.Input = new double[channle[0].Length][];
			for (int input = 0; input < channle[0].Length; input++) {
				data.Input[input] = new double[channle.Length];
				for (int time  = 0; time  < channle.Length; time ++) {
					if (channle[time][input]<=(Param.MaxLevel/4))
						data.Input[input][time] = channle[time][input];
					else
						data.Input[input][time] = 0;
				}
			}
		}
		//--------------------------------------------------------
		
		public void MRIinput_Method_1(ref globalParam Param, out double[][] data, ref double[,] inputData)
		{
			// input of 3.1234 , 4.8563 , 2.5864 wil be in chanelse
			//	time :    t1			,		t2				t3
			//		1: 3.1234 * factor	,	4.8563 * factor		,	2.5864  * factor
			//		2: 1 * factor		,	8 * factor			,	5  * factor
			//		3: 2 * factor		,	5 * factor			,	8  * factor
			//		4: 3 * factor		,	6 * factor			,	6  * factor
			//		5: 4 * factor		,	3 * factor			,	4  * factor
			
			int digits = Param.numbersAfterDot+Param.numbersBeforeDot;
			data = new double[1+digits][];
			int VoxelNumber = Param.iteration;
			int time = inputData.GetLength(0);
			double factor = Param.StreamFactor;
			for (int i = 0 ; i < data.Length ; i++) { data[i] = new double[time];	}
			
			// first channle
			for (int t = 0 ; t < time ; t++ ) {	data[0][t] = factor * inputData[t,VoxelNumber]; }
			// rest of the channles
			for (int t = 0 ; t < time ; t++ ){
				double[] temp = this.double2digits(inputData[t,VoxelNumber],Param.numbersBeforeDot,Param.numbersAfterDot);
				for (int x =0 ; x < digits ; x++){
					data[x+1][t] = factor * temp[x];
				}
			}
		}//------------------------------------------------------------
		
		
		public void MRIinput_Method_2(ref globalParam Param, out double[][] data, ref double[,] inputData)
		{
			// input of 3.1234 , 4.8563 , 2.5864 wil be in chanelse
			//	time :    t1			,		t2				,		t3
			//		1: 3.1234 * factor	,	3.1234 * factor		,	3.1234 * factor
			//		2: 4.8563 * factor	,	4.8563 * factor		,	4.8563 * factor
			//		3: 2.5864 * factor	,	2.5864 * factor		, 	2.5864 * factor

			data = new double[inputData.GetLength(0)][];
			int VoxelNumber = Param.iteration;
			double factor = Param.StreamFactor;
			for (int i = 0 ; i < data.Length ; i++) { data[i] = new double[1];	}
			// first channle
			for (int t = 0 ; t < data.Length ; t++ ) {	data[t][0] = factor * inputData[t,VoxelNumber]; }
		}//------------------------------------------------------------
		
		public void Normelize(ref globalParam.Data[] data,ref globalParam Param)
		{
			double max=0,min=0;
			for (int i = 0 ; i < data.Length ; i++ ) {
				for (int j=0 ; j < data[i].Input.Length ; j++ ) {
					for (int x=0 ; x < data[i].Input[j].Length ; x++ ) {
						if (max < data[i].Input[j][x]) max = data[i].Input[j][x];
						if (min > data[i].Input[j][x]) min = data[i].Input[j][x];
					}
				}
			}
			
			for (int i = 0 ; i < data.Length ; i++ ) {
				for (int j=0 ; j < data[i].Input.Length ; j++ ) {
					for (int x=0 ; x < data[i].Input[j].Length ; x++ ) {
						//data[i].Input[j][x] = data[i].Input[j][x]/divided;
						if (data[i].Input[j][x] >0)
							data[i].Input[j][x] = data[i].Input[j][x]/max;
						else
							data[i].Input[j][x] = data[i].Input[j][x]/min;
					}
				}
			}
		}//------------------------------------------------------------
		
		
		public void Normelize(ref globalParam Param)
		{
			this.Normelize(ref Param.LearnData, ref Param);
			this.Normelize(ref Param.TestData, ref Param);
			
		}//------------------------------------------------------------
		
		
		
		public double[][] digits2bin(double[] inNumber)
		{
			double[][] temp = new double[inNumber.Length][];
			
			for(int i = 0 ; i < inNumber.Length ; i++)
			{
				string tempNum = this.ToBinary((Int64)inNumber[i]);
				temp[i] = new double[tempNum.Length];
				for (int t = 0 ; t < tempNum.Length ; t++)
					if (tempNum[t]=='0')  temp[i][t] = 0;
				else temp[i][t] = 1;
			}
			
			return temp;
			
		}//------------------------------------------------------------
		
		
		public double[] double2digits(double inNumber,int numBeforeDot, int numAfterDot)
		{
			int counter = numAfterDot+numBeforeDot;
			double[] outnumber = new double[counter];
			double number = inNumber;
			
			for(int i = numBeforeDot ; i > 0 ; i--){
				counter--;
				outnumber[counter] = (int) (number/(Math.Pow(10,i)));
				number = (number%(Math.Pow(10,i)));
			}
			
			number = inNumber*Math.Pow(10,numAfterDot);
			
			for(int i = numAfterDot ; i > 0 ; i--){
				counter--;
				outnumber[counter] = (int) (number/(Math.Pow(10,i)));
				number = (number%(Math.Pow(10,i)));
			}
			return outnumber;
		}//------------------------------------------------------------
		
		public int ToDecimal(string bin)
		{
			return Convert.ToInt32(bin,2);
		}
		//--------------------------------------------------------
		
		
		public string[] ToUnary(int num,double spike)
		{
			string[]  returnNum = new System.String[num];
			for (int i = 0 ; i < num ; i++)
				returnNum[i] = spike.ToString();
			
			return returnNum;
		}
		//--------------------------------------------------------
		
		
		
		public string ToBinary(Int64 Decimal)
		{
			// Declare a few variables we're going to need
			Int64 BinaryHolder;
			char[] BinaryArray;
			string BinaryResult = "";
			
			if ( Decimal==0) {
				BinaryResult="0";
			}

			while (Decimal > 0)
			{
				BinaryHolder = Decimal % 2;
				BinaryResult += BinaryHolder;
				Decimal = Decimal / 2;
			}
			// The algoritm gives us the binary number in reverse order (mirrored)
			// We store it in an array so that we can reverse it back to normal
			BinaryArray = BinaryResult.ToCharArray();
			Array.Reverse(BinaryArray);
			BinaryResult = new string(BinaryArray);

			return BinaryResult;
		}
		//--------------------------------------------------------
		
		
	}

}
