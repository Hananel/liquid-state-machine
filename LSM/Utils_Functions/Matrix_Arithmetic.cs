﻿/*
 * Created by SharpDevelop.
 * User: hhazan01
 * Date: 24/05/2010
 * Time: 19:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Utils_Functions
{
	/// <summary>
	/// Description of Matrix_Arithmetic.
	/// </summary>
	public class Matrix_Arithmetic
	{
		public Matrix_Arithmetic()
		{}
		
		public double Max(double[][] Matrix)
		{
			double max=Matrix[0][0];
			for (int i = 0 ; i < Matrix.Length ; i++) {
				for (int j = 0 ; j < Matrix[i].Length ; j++ ) {
					if (max<Matrix[i][j]) max = Matrix[i][j];
				}
			}
			return max;
		}
		
		public double Min(double[][] Matrix)
		{
			double min=Matrix[0][0];
			for (int i = 0 ; i < Matrix.Length ; i++) {
				for (int j = 0 ; j < Matrix[i].Length ; j++ ) {
					if (min>Matrix[i][j]) min = Matrix[i][j];
				}
			}
			return min;
		}
		
		public double Max(double[] Matrix)
		{
			double max=Matrix[0];
				for (int j = 0 ; j < Matrix.Length ; j++ ) 
					if (max<Matrix[j]) max = Matrix[j];
			return max;
		}
		
		public double Min(double[] Matrix)
		{
			double min=Matrix[0];
				for (int j = 0 ; j < Matrix.Length ; j++ ) 
					if (min > Matrix[j]) min = Matrix[j];
			return min;
		}
		
		public int Max(int[][] Matrix)
		{
			int max=Matrix[0][0];
			for (int i = 0 ; i < Matrix.Length ; i++) {
				for (int j = 0 ; j < Matrix[i].Length ; j++ ) {
					if (max<Matrix[i][j]) max = Matrix[i][j];
				}
			}
			return max;
		}
		
		public int Min(int[][] Matrix)
		{
			int min=Matrix[0][0];
			for (int i = 0 ; i < Matrix.Length ; i++) {
				for (int j = 0 ; j < Matrix[i].Length ; j++ ) {
					if (min>Matrix[i][j]) min = Matrix[i][j];
				}
			}
			return min;
		}
		
		public int Max(int[] Matrix)
		{
			int max=Matrix[0];
				for (int j = 0 ; j < Matrix.Length ; j++ ) {
					if (max<Matrix[j]) max = Matrix[j];
				}
			return max;
		}
		
		public int Min(int[] Matrix)
		{
			int min=Matrix[0];
				for (int j = 0 ; j < Matrix.Length ; j++ ) {
					if (min>Matrix[j]) min = Matrix[j];
				}
			return min;
		}
		
		public double[] Vector_Average(double[][] Matrix)
		{
			double[] sum = new double[Matrix.Length];
			for (int i = 0 ; i < Matrix.Length ; i++ ) {
				int counter=0;
				sum[i] = 0 ;
				for (int t = 0 ;  t < Matrix[i].Length ; t++ ) {
					sum[i] += Matrix[i][t];
					counter++;
				}
				sum[i] = sum[i] / counter;
			}
			
			return sum;
		}
		//-------------------------------------------------
		
		public double Vector_Average(double[] Matrix)
		{
			double sum = 0;
			for (int i = 0 ; i < Matrix.Length ; i++ ) {
				sum += Matrix[i];
			}
			sum = sum / Matrix.Length;
			return sum;
		}
		//-------------------------------------------------
		
		public void AddCell(ref int[] vector,int num)
		{
			int[] tempVec = new int[vector.Length];
			vector.CopyTo(tempVec,0);
			vector = new int[tempVec.Length+1];
			tempVec.CopyTo(vector,0);
			vector[tempVec.Length] = num;
		}
		//-------------------------------------------------
		
		public void delNum(ref int[] vector,int num)
		{
			int sum=0,counter=0;
			for (int i = 0 ; i < vector.Length ; i++ ) 
				if (num==vector[i]) sum++;
			
			int[] tempVec = new int[vector.Length-sum];
			for (int i = 0 ; i < vector.Length ; i++ ) 
				if (num!=vector[i]) {
				tempVec[counter]=vector[i];
				counter++;
			}
			vector = new int[tempVec.Length];
			tempVec.CopyTo(vector,0);
		}
		//-------------------------------------------------
		
		public void replicateVectorWithoutNum(ref int[] sourceVector,ref int[] targetgVector,int num)
		{
			int sum=0,counter=0;
			for (int i = 0 ; i < sourceVector.Length ; i++ ) 
				if (num==sourceVector[i]) sum++;
			
			targetgVector = new int[sourceVector.Length-sum];
			
			for (int i = 0 ; i < sourceVector.Length ; i++ ) 
				if (num!=sourceVector[i]) {
				targetgVector[counter]=sourceVector[i];
				counter++;
			}
		}
		//-------------------------------------------------
		
		public void replicateVectorWithoutNum(ref int[] sourceVector,ref int[] targetgVector,int num,int from,int until)
		{
			int sum=0,counter=0;
			for (int i = from ; i < until ; i++ ) 
				if (num==sourceVector[i]) sum++;
			
			targetgVector = new int[(until-from)-sum];
			
			for (int i = from ; i < until ; i++ ) 
				if (num!=sourceVector[i]) {
				targetgVector[counter]=sourceVector[i];
				counter++;
			}
		}
		//-------------------------------------------------
		
		public void replicateVector(ref int[] sourceVector,ref int[] targetgVector,int from,int until)
		{
			targetgVector = new int[until-from];
			
//			Array.Copy(sourceVector,from,targetgVector,from,targetgVector.Length);
			
			 for (int i = from ; i < until ; i++ ) 
					targetgVector[i] = sourceVector [i];
		}
		//-------------------------------------------------
		
		
	}
}
