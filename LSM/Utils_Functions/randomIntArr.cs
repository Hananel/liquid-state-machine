﻿/*
 * Created by SharpDevelop.
 * User: Hananel
 * Date: 11/08/2009
 * Time: 14:48
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace Utils_Functions
{
	/// <summary>
	/// Description of randomIntArr.
	/// </summary>
	public class randomIntArr
	{
		public Random RandomNum;
		
		public randomIntArr (int seed)
		{
			RandomNum  = new Random(seed);
		}
		
		public void select(int min, int max, ref int[] arr)
		{
			int[] temp = new int[max-min];
			for (int count=0, i = min ; i < max  ; count++ , i++ ) { temp[count]=i;}
			do{
				this.shuffle(ref temp);
			}while (RandomNum.NextDouble()<0.5);
			for (int i=0 ; i < arr.Length ; i++ ) {
				arr[i] = temp[i];
			}
		}
		
		public void select(int min, int max, ref long[] arr)
		{
			long[] temp = new long[max-min];
			for (long count=0, i = min ; i < max  ; count++,i++ ) { temp[count]=i;}
			do{
				this.shuffle(ref temp);
			}while (RandomNum.NextDouble()<0.5);
			for (long i=0 ; i < arr.Length ; i++ ) {
				arr[i] = temp[i];
			}
		}
		
		
		public void dselect(int min, int max, int dontMin, int dontMax, ref int[] arr)
		{
			int[] temp 	= new int[(max-min)-(dontMax-dontMin)];
			for (int i=min,count=0 ; i < max ; i++ ) {
				if ((i>=dontMin)&&(i<=dontMax)) continue;
				temp[count] = i;
				count++;
			}
			
			do{
				this.shuffle(ref temp);
			}while (RandomNum.NextDouble()<0.5);
			
			for (int i=0 ; i < arr.Length ; i++ ) {
				arr[i] = temp[i];
			}
		}
		
		public void dselect(int min, int max, int[] dont, ref int[] arr)
		{
			int[] temp 	= new int[((max-min)-dont.Length)+1];
			for (int i=min,count=0 ; i < max ; i++ ) {
				int flag=0;
				for (int t=0 ; t < dont.Length ; t++ )
					if (i==dont[t]) flag=1;
				if (flag==1) continue;
				temp[count] = i;
				count++;
			}
			
			do{
				this.shuffle(ref temp);
			}while (RandomNum.NextDouble()<0.5);
			
			for (int i=0 ; i < arr.Length ; i++ ) {
				arr[i] = temp[i];
			}
		}
		
		public void dselect(int min, int max, int dont, ref int[] arr)
		{
			int[] temp 	= new int[(max-min)];
			for (int i=min,count=0 ; i <= max ; i++ ) {
				if (i==dont) continue;
				if (count==temp.Length) break;
				temp[count] = i;
				count++;
			}
			
			do{
				this.shuffle(ref temp);
			}while (RandomNum.NextDouble()<0.5);
			
			for (int i=0 ; i < arr.Length ; i++ ) {
				arr[i] = temp[i];
			}
		}
		
		//--------------------------------------------------------------------------------------
		
		public void shuffle(ref int[] arr)
		{
			int times = 3+RandomNum.Next(1,Math.Max(2,arr.Length/100));
			for ( int i=0;i<times ; i++) {
				for(int n=arr.Length ; n>1 ; n--)
				{
					int rand_ind=RandomNum.Next(0,n-1); // function returns element in [0,n-1], it may be rand()%n.
					int temp = arr[rand_ind];
					arr[rand_ind] = arr[n-1];
					arr[n-1] = temp;
				}
			}
		}
		//--------------------------------------------------------------------------------------
		
		public void shuffle(ref int[] arr,int from,int until)
		{
			int times = 3+RandomNum.Next(1,Math.Max(2,arr.Length/100));
			for ( int i=0;i<times ; i++) {
				for(int n = until ; n>1 ; n--)
				{
					int rand_ind=RandomNum.Next(from,n-1); // function returns element in [0,n-1], it may be rand()%n.
					int temp = arr[rand_ind];
					arr[rand_ind] = arr[n-1];
					arr[n-1] = temp;
				}
			}
		}
		//--------------------------------------------------------------------------------------
		
		public void shuffle(ref long[] arr)
		{
			int times = 3+ RandomNum.Next(1,Math.Max(2,arr.Length/100));
			for ( int i=0;i<times ; i++) {
				for(int n=arr.Length ; n>1 ; n--)
				{
					int rand_ind=RandomNum.Next(0,n-1); // function returns element in [0,n-1], it may be rand()%n.
					long temp = arr[rand_ind];
					arr[rand_ind] = arr[n-1];
					arr[n-1] = temp;
				}
			}
		}
		//--------------------------------------------------------------------------------------
		
		public void random_power_low_sellect_Mathod1(int minimum_connection,int[] nodesList,int howMuchToSellect,int extend, out int[] list,out int[] countingList)
		{
			// oreginal one
			list = new int[howMuchToSellect];
			countingList = new int[nodesList.Length];
			int additionalSize=(howMuchToSellect*extend);
			int nodesListSize=nodesList.Length;
			for ( int i=0 ; i<countingList.Length ;  i++) {countingList[i]=0;}
			
			Array.Resize(ref nodesList,nodesListSize+additionalSize);
			for ( int mone=0; mone < howMuchToSellect ; mone++ ) {
				int candidant = nodesList[RandomNum.Next(0,nodesListSize)];
				for ( int i = nodesListSize ; i < (nodesListSize+extend) ;i++ ) { nodesList[i] = candidant;	}
				list[mone] = candidant;
				nodesListSize+= extend;
			}
			//------------------------------------
			
			for ( int i=0 ; i<countingList.Length ;  i++) {countingList[i]=0;}
			long sellectCounter = 0;
			long[] temp = new long[list.Length];
			list.CopyTo(temp,0);
			Array.Sort(temp);
			long lastNumber = temp[0];
			for ( int i=0 ; i<temp.Length ;  i++) {
				countingList[sellectCounter]++;
				if (lastNumber!=temp[i]) { lastNumber=temp[i]; sellectCounter++;}
			}
			
			nodesList = new int[1];
			nodesList = null;
		}
		
	}
	
	public class RandomPowerLaw
	{
		Random RandomNum;
		randomIntArr randomMathods;
		Matrix_Arithmetic VectorMethods;
		int[] nodesListCounter,LeftOverList;
		int candidate,listSize,listSize_org,extend,extend_org;
		public int counter,nodes;

		
		public RandomPowerLaw(int NumOfnodesList, int extend)
		{
			this.listSize_org = NumOfnodesList;
			this.extend_org = extend;
			this.nodes = NumOfnodesList;
			this.Rest();
		}
		
		public RandomPowerLaw(int NumOfnodesList, int extend,int[] connections)
		{
			this.listSize_org = NumOfnodesList;
			this.extend_org = extend;
			this.nodes = NumOfnodesList;
			this.Rest(connections);
		}
		
		public void Rest(int[] connections){
			this.listSize = this.listSize_org;
			this.extend = this.extend_org;
			
			int Seed = Convert.ToInt32((System.DateTime.UtcNow.Ticks)%int.MaxValue);
			VectorMethods = new Matrix_Arithmetic();
			RandomNum  = new Random(Seed);
			randomMathods = new randomIntArr( RandomNum.Next() );
			
			nodesListCounter = new int[listSize];
			LeftOverList = new int[listSize];
			for (int i=0 ; i < listSize ; i++ ){
				nodesListCounter[i] = connections[listSize-i-1];
			}
			this.NewNode();
		}
		
		public void Rest(){
			this.listSize = this.listSize_org;
			this.extend = this.extend_org;
			
			int Seed = Convert.ToInt32((System.DateTime.UtcNow.Ticks)%int.MaxValue);
			VectorMethods = new Matrix_Arithmetic();
			RandomNum  = new Random(Seed);
			randomMathods = new randomIntArr( RandomNum.Next() );
			
			nodesListCounter = new int[listSize];
			LeftOverList = new int[listSize];
			for (int i=0 ; i < listSize ; i++ ){
				nodesListCounter[i] = 1;
				LeftOverList[i] = i;
			}
			randomMathods.shuffle(ref LeftOverList);
		}
		
		public void NewNode(){
			int lengh=0;
			for (int i = 0; i < nodesListCounter.Length ; i++ ) { lengh+=(nodesListCounter[i]-1)*this.extend_org; }
			LeftOverList = new int[lengh+nodesListCounter.Length];
			int count=0;
			for (int i=0 ; i < nodesListCounter.Length ; i++ )
				for (int t = 0 ; t < ((nodesListCounter[i]-1)*this.extend_org)+1 ; t++ ) {
				LeftOverList[count] = i;
				count++;
			}
			randomMathods.shuffle(ref LeftOverList);
			this.counter=0;
		}
		
		public bool Next(ref int candid){
			if (LeftOverList.Length==0) return false;
			this.candidate = RandomNum.Next(0,LeftOverList.Length);
			this.candidate = LeftOverList[this.candidate];
			candid = this.candidate;
			return true;
		}
		
		public bool Next(ref int candid,int min,int max){
			if (LeftOverList.Length==0) return false;
			do{
				this.candidate = LeftOverList[RandomNum.Next(0,LeftOverList.Length)];
			}while(!(this.candidate>min)&&(this.candidate<max));
			candid = this.candidate;
			return true;
		}
		
		public void NotGood()
		{
			VectorMethods.delNum(ref LeftOverList,this.candidate);
		}
		
		public void Good()
		{
			this.nodesListCounter[this.candidate]++;
			VectorMethods.delNum(ref LeftOverList,this.candidate);
			counter++;
		}
		
	}
}