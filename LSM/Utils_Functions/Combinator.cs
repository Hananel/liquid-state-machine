﻿/*
 * Created by SharpDevelop.
 * User: HH
 * Date: 08/01/2010
 * Time: 11:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Utils_Functions
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class Combinator
	{
		int[] numOFgroups,selectGroup;
		int max;
		public Combinator(int[] Group)
		{
			this.numOFgroups = Group;
			this.max = Group[0];
			for(int i=0 ; i<Group.Length  ; i++){ max=Math.Max(Group[i],max); }
		}
		
		public static Int64 factorial(int i)
		{
			return((i <= 1) ? 1 : (i * factorial(i-1)));
		}
		
		void next(int begin)
		{
			if (begin==0){
				for (int i=0 ; i<this.selectGroup.Length ; i++ )
					this.selectGroup[i]=i; // "pointer" to the cell in the array
			}else{
				for (int i=selectGroup.Length-1 ; i > -1 ; i--) {
					if (this.numOFgroups.Length-1>this.selectGroup[i]) {
						int flag=0;
						this.selectGroup[i]++;
						for(int t=i+1; t<selectGroup.Length ; t++)
							if (this.selectGroup[t-1]+1<=this.max) {	this.selectGroup[t] = this.selectGroup[t-1]+1;
						}else{
							flag =1 ; continue;}
						if (flag==0)	break;
					}
					
				}
			}
			
		}
		
		public int[][] Select(int NumOFgroupsTOSelect)
		{
			/// return the all combination of chosen n from m group
			/// 
			int[][] output = new int[0][];
			// if select number is bigger then the group number EXIT
			if (NumOFgroupsTOSelect>this.numOFgroups.Length) return output;
			
			this.selectGroup = new int[NumOFgroupsTOSelect];
			
			output = new int[factorial(this.numOFgroups.Length)/(factorial(NumOFgroupsTOSelect)*factorial(this.numOFgroups.Length-NumOFgroupsTOSelect))][];
			
			int counter=0;
			
			for (int frac=0 ; frac < output.Length ; frac++){
				int mone=1;
				int[] tempSellect = new int[NumOFgroupsTOSelect];
				while (mone>0) {
					mone=0;
					this.next(frac);
					for(int i = 0 ; i<NumOFgroupsTOSelect ; i++){tempSellect[i]=this.numOFgroups[this.selectGroup[i]];}
				}
				output[counter] = tempSellect;
//			for(int i = 0 ; i<NumOFgroupsTOSelect ; i++){Console.Write(" {0} ",tempSellect[i].ToString());}
//			Console.WriteLine("");
				counter++;
			}
			return output;
		}
		
		public int[][] fill(int[][] one)
		{
			/// The opposite of sellect..
			/// retun the other groups that "sellect" didnt choose
			int[][] output = new int[one.Length][];
			
			for(int i = 0; i<one.Length ; i++)
			{
				output[i] = new int[this.numOFgroups.Length-one[i].Length];
				int mone=0,moneOut=0;
				for (int t=0 ; t<this.numOFgroups.Length ; t++) {
					if (this.numOFgroups[t]==one[i][mone]) {if (mone<one[i].Length-1)mone++;}
					else {output[i][moneOut] = this.numOFgroups[t]; moneOut++;}
				}
			}
			
			
			return output;
		}

	}

}