/*
 * Created by SharpDevelop.
 * User: hhazan01
 * Date: 06/05/2009
 * Time: 14:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using NeuronNetwork;
using Inputs;
using System;
using System.Threading;
using System.Windows.Forms;

namespace Liquid
{

	public class LSM
	{

		public int Netsize;
		public int InputSize;
		public int TimeInteval;
		public int NetOutputSize;
		public double[] ConnectionDistribution_Input,ConnectionDistribution_Output;
		NeuronNetwork.Network LSMnet;
		ReadOut_Detector NN;

		public LSM(ref globalParam Param)
		{
			this.TimeInteval = Param.LSM_Runing_Interval;
			this.Netsize=Param.Number_Of_Neurons;
			ConnectionDistribution_Input = new double[this.Netsize];
			ConnectionDistribution_Output = new double[this.Netsize];
			this.LSMnet = new Network(ref this.NetOutputSize , ref Param, ref this.ConnectionDistribution_Input,ref this.ConnectionDistribution_Output,ref InputSize);
			// init the Detector
			this.NN = new ReadOut_Detector(this.NetOutputSize,((int) Math.Round(this.NetOutputSize*Param.ReadOut_Unit_HiddenLayerSize)),Param.ReadOut_Unit_outputSize,ref Param);
			// finish the Detector
			
		}//--------------------------------------------------------------------

		public void run(ref globalParam.Data[] LearnVec , out double[][,] OutputVectot,out double[][,] inputNueronsVec, int NorGorD, double percent,ref globalParam Param)
		{
			// NorGorD =-> 0) Generalization Test 1) Normal behaviral 2) Noise Damage 3) Dead Damage
			
			OutputVectot = new double[LearnVec.Length][,];
			inputNueronsVec = new double[LearnVec.Length][,];
			
			// Start run the LSM on the inputs
			LSMnet.reset(ref Param);
			for (int i = 0; i <  LearnVec.Length ; i++){
				OutputVectot[i] = new double[this.NetOutputSize, this.TimeInteval];
				inputNueronsVec[i] = new double[this.Netsize-this.NetOutputSize, this.TimeInteval];
				LSMnet.reset(ref Param);
				LSMnet.run_on_vector(ref LearnVec[i].Input, ref OutputVectot[i], ref inputNueronsVec[i], this.TimeInteval , NorGorD, percent, ref Param);
			}

		}//--------------------------------------------------------------------
		
		public void run_TimeTest(ref DamageLSM.Input LearnVec,ref double[] Target, ref double[][,] OutputVectot,ref double[][,] inputNueronsVec, ref double[] OutputTargetVecore, ref double[][,] Activity,ref globalParam Param)
		{
			// Start run the LSM on the inputs
			LSMnet.reset(ref Param);
			for (int i = 0; i < LearnVec.HowManyNumbers; i++){
				OutputVectot[i] = new double[this.NetOutputSize, Param.Activity_Test_Running_Time];
				inputNueronsVec[i] = new double[this.Netsize-this.NetOutputSize, Param.Activity_Test_Running_Time];
				OutputTargetVecore[i] = Target[i % Target.Length];
				Activity[i] = new double[2,Param.Activity_Test_Running_Time];
				for ( int t = 0; t< Param.Activity_Test_Running_Time ; t++ ) { Activity[i][0,t]=0;	Activity[i][1,t]=0;}
				LSMnet.reset(ref Param);
				LSMnet.TimeStep(i,ref LearnVec, ref OutputVectot[i], ref inputNueronsVec[i], ref Activity[i], ref Param);
			}

		}//--------------------------------------------------------------------

		public void PrintNetwork()
		{
			LSMnet.PrintNetwork();
		}//--------------------------------------------------------------------
		
		public void DOTFile()
		{
			LSMnet.DOTFile();
		}//--------------------------------------------------------------------
		
		
		public double Learn(ref globalParam Param, ref globalParam.Data[] LearnData,int print)
		{
			double[][,] LiquidOutput_InputUnits_Learn;
			double[][,] LiquidOutput_OutputUnits_Learn;
			this.run( ref LearnData , out LiquidOutput_OutputUnits_Learn, out LiquidOutput_InputUnits_Learn, 1, 0.0 , ref Param);
			
			// Training Detector BY Sliceing Time
			double LastReturnError=0;
			NN.StratTrain(ref LiquidOutput_OutputUnits_Learn,ref LearnData,ref LastReturnError ,ref Param);
			
			if (print==1){
				// ploting the activity of the liquid
				for (int i = 0 ; i <LiquidOutput_InputUnits_Learn.Length ; i++){
					plotxy p = new plotxy();
					p.loadData(ref LiquidOutput_OutputUnits_Learn[i],ref LiquidOutput_InputUnits_Learn[i],ref Param );
					Application.Run( p );
					Application.Exit();
				}
			}

			return LastReturnError;
		}//--------------------------------------------------------------------

		
		public void Test(ref globalParam Param, ref globalParam.Data[] testData , out double[][] DetectorOutput,int print)
		{
			double[][,] LiquidOutput_InputUnits_Test;
			double[][,] LiquidOutput_OutputUnits_Test;
			this.run( ref testData , out LiquidOutput_OutputUnits_Test, out LiquidOutput_InputUnits_Test, 1, 0.0 , ref Param);
			
			if (print==1){
				// ploting the activity of the liquid
				for (int i = 0 ; i <LiquidOutput_OutputUnits_Test.Length ; i++){
					plotxy p = new plotxy();
					p.loadData(ref LiquidOutput_OutputUnits_Test[i],ref LiquidOutput_OutputUnits_Test[i],ref Param );
					Application.Run( p );
					Application.Exit();
				}
			}
			
			// testing Detector
			Console.WriteLine("");
			Console.WriteLine("Start Normal Testing Vectors");
			NN.StratTesting(ref LiquidOutput_OutputUnits_Test, out DetectorOutput,ref Param);
			
		}//--------------------------------------------------------------------

	}

}
