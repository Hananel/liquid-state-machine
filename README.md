### master branch ###
The master branch contain the source code for The Liquid State Machine (LSM) to produce the results that published at Expert Systems with Applications, [Topological constraints and robustness in liquid state machines](http://cs.haifa.ac.il/~hhazan01/Publication/2012/Expert%20SystemV.3.7.pdf), Volume 39, Issue 2, Pages 1597-1606, dx.doi.org/10.1016

The Liquid State Machine (LSM) is a method of computing with temporal neurons, which can be used amongst other things for classifying intrinsically temporal data directly unlike standard artificial neural networks. It has also been put forward as a natural model of certain kinds of brain functions. 

With this code we show that the Liquid State Machines as normally defined  by Maass et al cannot serve as a natural model for brain function. This is because they are very vulnerable to failures in parts of the model. 

This code solve this issue by specifying certain kinds of topological constraints (such as “small world assumption”), which have been claimed are reasonably plausible biologically, can restore robustness in this sense to LSMs.

### Requirements ###
1. Encog library 2.5 https://github.com/encog/encog-dotnet-core
2. ZedGraph http://sourceforge.net/projects/zedgraph/files/